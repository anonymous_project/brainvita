from django.contrib import admin
from models import *
from profiles.models import *
#admin.site.register(ContentType)
admin.site.register(ContentAnalysis)
admin.site.register(Project)
admin.site.register(Media)
admin.site.register(MediaTesterMapping)
admin.site.register(MediaTest)
admin.site.register(MailActivation)
admin.site.register(MediaTestHashtag)
admin.site.register(MindWave)
admin.site.register(Image)
admin.site.register(Admin)
admin.site.register(HashTag)
admin.site.register(Category)
admin.site.register(SubCategory)
admin.site.register(ContentUpload)
admin.site.register(LoginCreate)
admin.site.register(Video)
admin.site.register(VideoImage)
admin.site.register(Login)
admin.site.register(Profile)
# Register your models here.
