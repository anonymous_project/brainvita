from django.conf import settings
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from django.template.loader import get_template
from django.template import RequestContext,Context
import hashlib,random,smtplib
from django.views import generic
from . import forms
from models import User
from braces import views as bracesviews
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.contrib import messages
from models import MailActivation

def generate_activation_key(username):
    """
    Generation of activation key using random salt based on username
    @input username
    @output activation_key
    """

    salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
    usernamesalt = username
    if isinstance(usernamesalt, unicode):
        usernamesalt = usernamesalt.encode('utf8')
    activation_key = hashlib.sha1(salt+usernamesalt).hexdigest()
    return activation_key

def send_activatation_email(request,activation_key,name,user_email):
    """
    Sending activation code via email
    @input activation_key, user_email
    """
    data = dict()
    protocol ='https://' if request.is_secure() else 'http://'
    host = request.get_host()
    # data['body'] ="Activate your chromo using this link " + protocol+request.get_host() +'/activate/'+activation_key
    # sending params to the template
    params= RequestContext(request,{'protocol':protocol,'host':host,'activation_key':activation_key,
                                    'name':name,'email':user_email})
    message = get_template('dashboard/activate_email.html').render(params)
    data['html'] = message
    data['subject']= "Action required: Confirm your AffectLab account"
    data['to']= user_email
    send_email(data)

def send_email(data):
    """
    Generalized Send Email Function which sends email
     @input to_address, subject, body ,html tag
    """
    fromaddr = settings.DEFAULT_FROM_EMAIL
    toaddrs  = data['to']
    message = MIMEMultipart()
    message['from'] = fromaddr
    message['subject'] = data['subject']
    message['to'] = data['to']
    if data.get('html'):
        part = MIMEText(data['html'], 'html')
        message.attach(part)
    else:
        message.attach(MIMEText(data['body'], 'plain'))
    smtp_server = settings.EMAIL_HOST
    smtp_username =settings.EMAIL_HOST_USER
    smtp_password = settings.EMAIL_HOST_PASSWORD
    smtp_port = settings.EMAIL_PORT
    smtp_do_tls = True

    server = smtplib.SMTP(
        host = smtp_server,
        port = smtp_port,
        timeout = 10
    )
    server.starttls()
    server.ehlo()
    server.login(smtp_username, smtp_password)
    server.sendmail(fromaddr, toaddrs, message.as_string())
    server.quit()


class SignUpView(bracesviews.AnonymousRequiredMixin,
                 bracesviews.FormValidMessageMixin,
                 generic.CreateView):
    form_class = forms.SignupForm
    model = User
    template_name = 'dashboard/activate_email.html'
    success_url = reverse_lazy(settings.API_HOSTNAME+'login/')
    #form_valid_message = "You're signed up" #, Please Check your activation link in E-Mail and then login!"
    def activate_user(self, request, key):
        """
        Perform Activation of user
        """
        redirect_url = settings.API_HOSTNAME+"login/"
        mailactivation = get_object_or_404(MailActivation, activation_key=key)
        if mailactivation.email_verified == False:
            mailactivation.email_verified = True
            mailactivation.user.is_active = True
            mailactivation.user.save()
            mailactivation.save()
            messages.success(request, "Your Account has been Activated")
        else:
            redirect_url = settings.API_HOSTNAME+"login/"
            messages.error(request, "Your Account is already active")
        return redirect(redirect_url)


