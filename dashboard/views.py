from django.shortcuts import render
from django.views.generic import TemplateView
from django.http import HttpResponse
from django.db.models import Max
from dateutil import parser
import copy,requests
from requests.auth import HTTPBasicAuth
from socket_connector import *
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from django.shortcuts import render_to_response
from rest_framework import status
from rest_framework.views import APIView
from django.http import JsonResponse
from serializers import *
from django.conf import settings
from collections import defaultdict
from django.http import QueryDict
# from rest_framework.authentication import BasicAuthentication,SessionAuthentication
# from authentication import *
from dateutil.relativedelta import relativedelta
from models import *
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from mail_utils import send_activatation_email, send_email
from braces import views as bracesviews
from django.views import generic
from django.views.generic import ListView
from django.core.urlresolvers import reverse_lazy
from mail_utils import generate_activation_key
from django.shortcuts import get_object_or_404
from . import forms
from profiles.models import Profile
from io import StringIO
from datetime import timedelta
import json, numpy as np, pandas as pd, os, smtplib,string ,datetime,random, zipfile, sys, traceback, threading,re,uuid,csv,pytz


class AvgGraph1(APIView):

    def __init__(self):
        self.default_age = {29: {"birth_date__lt": self.get_age_date(29)},
                            50: {"birth_date__gt": self.get_age_date(50)},
                            49: {"birth_date__range": self.get_age_date([50,29])}}
        self.age_map = {29:"0-29", 49:"29-49" ,50 : ">50"}
        self.emotion_columns = ["Bored","Angry","Sad","Excited","Happy","Content"]
        #self.emotion_columns1 = ["Tense", "Sad", "Happy", "Relaxed"]
        self.emotion_columns = ["Tense", "Relaxed", "Sad", "Neutral", "Happy"]
        self.default_select = ["attention", "ET", "YY", "ME","AP"]
        self.hl_columns = ['high','low','medium','very high']
        self.emotion_columns1 = ["Tense", "Relaxed", "Sad", "Neutral", "Happy"]

    def emo_total(self,df,columns):
        total = 0
        for i in columns:
            total+=df[i]
        return total

    def stacked(self,emo,metric="avg",default_columns=None):
        columns = list(emo.columns)
        emo=emo.reset_index()
        emo["total"] = emo.apply(lambda x : self.emo_total(x,default_columns),axis=1)

        for i in default_columns:
            emo[i]  = (emo[i]*100)/emo["total"]

        columns = ["second"] + columns + ["total"]

        emo.columns = columns
        emo.second = emo.second.round()
        del emo['total']
        emo = emo.fillna(0).T.to_dict()
        emo = [emo[k] for k in emo.keys()]
        return emo

    def aggregate_groups(self,df,num_groups,metric="avg",duration = None):
        duration = self.duration if not duration else duration
        gap = float(duration)/num_groups
        if gap> 1:
            ranges  = map(lambda x : int(x),[(gap * i) for i in range(num_groups + 1)])
        else:
            ranges = range(duration+1)
        averages = {}
        if metric =="avg":
            averages = df.groupby(pd.cut(df['second'],ranges)).mean()
        else:
            averages = df.groupby(pd.cut(df['second'], ranges)).std()
        averages['second'] = map(lambda x : eval(str(x).replace(')',']').replace('(','['))[1],list(averages.index))
        return averages

    def transform2rickshaw(self,dictionary,select):
        transform = {}
        for c in select:
            transform[c]=[{'x':k,'y': dictionary[c][k],
            'vws':dictionary['vws'][k],} for k in dictionary[c].keys()]
        return transform

    def general_graphs(self,df,metric="avg",select=None):
        if metric == "avg":
            mean = df.groupby('second').mean()
        else:
            mean = df.groupby('second').std()

        mean['vws'] = df.groupby('second').size()
        mean['x'] = mean.index
        selected = mean.to_dict()
        select = self.default_select if not select else select
        return self.transform2rickshaw(selected,select)

    def avg_graph_separated(self,data,num_groups= 10,metric="avg",select=None):
        df = pd.DataFrame(data).fillna(0)
        self.duration1 = max(df['second'])
        select = self.default_select if not select else select
        means = {}
        for c in select:
            if metric=="avg":
                means[c]=np.mean(df[c])
            else:
                means[c] = np.std(df[c])
        return means

    def aggregatorAcrossSecondGroups(self,data,data1,data2,num_groups= 10,metric="avg",select=None,graph=1):
        df = pd.DataFrame(data).fillna(0)
        if not df.shape[0]:
            if graph:
                return {'graph':{}}
            else:
                return self.default_result(select)
        self.duration = max(df['second'])

        select = select if select else self.default_select
        if graph == 1:
            general_graph = self.general_graphs(df, metric, select)
            return {'graph':general_graph}

        df1 = pd.DataFrame(data1).fillna(0)
        self.duration1 = max(df1['second'])
        df2 = pd.DataFrame(data2).fillna(0)
        self.duration2 = max(df2['second'])
        hl = None
        for s in select:
            if not isinstance(hl,pd.DataFrame):
                hl=pd.DataFrame(df2.apply(lambda x: self.high_low(x,s), axis=1), columns=[s])
                continue
            hl[s] = pd.DataFrame(df2.apply(lambda x: self.high_low(x,s), axis=1), columns=[s])

        emo_df1 = pd.DataFrame(df2.apply(lambda x: self.make_emotion3(x), axis=1).tolist(), columns=["emotion",'second'])
        emo_df = pd.DataFrame(df2.apply(lambda x: self.make_emotion3(x), axis=1).tolist(), columns=["emotion","second"])

        total_emotion1 =  emo_df1.groupby('emotion').size().sum()
        total_emotion = emo_df.groupby('emotion').size().sum()
        radar = (emo_df.groupby('emotion').size()*100/float(total_emotion)).to_dict()
        for eg in self.emotion_columns :
            radar[eg] = 0 if eg not in radar else radar[eg]

        radar1 = (emo_df1.groupby('emotion').size()*100/float(total_emotion1)).to_dict()
        for eg in self.emotion_columns1 :
            radar1[eg] = 0 if eg not in radar1 else radar1[eg]

        hl['second'] = df2['second']
        hl_dictionary = {}
        for s in select:
            hl_dictionary[s] = hl[[s,'second']]
            #hl_dictionary[s] = hl[['second']].groupby(['second',s]).size()

            hl_dictionary[s]=pd.pivot_table(pd.DataFrame(hl_dictionary[s]).reset_index(level=['second', s]),
                           index="second", columns=s, aggfunc="count", fill_value=0)
            hl_dictionary[s].columns = hl_dictionary[s].columns.droplevel(0)

            hl_dictionary[s].reset_index(['second']+list(hl_dictionary[s].columns))
            for i in self.hl_columns:
                if i not in hl_dictionary[s].columns:
                    hl_dictionary[s][i] = 0

            hl_dictionary[s]['second'] = hl_dictionary[s].index

            hl_dictionary[s].index = range(1,hl_dictionary[s].shape[0]+1)
            groups =  20 if hl_dictionary[s].shape[0]>=20 else hl_dictionary[s].shape[0]
            hl_dictionary[s] = self.aggregate_groups(hl_dictionary[s], groups,duration=self.duration2,metric="avg")
            hl_dictionary[s].rename(index= {'second':'index'})

            hl_dictionary[s].index = hl_dictionary[s]['second']

            del hl_dictionary[s]['second']
            hl_dictionary[s] = hl_dictionary[s].reset_index()
            distribution  = self.stacked(hl_dictionary[s],default_columns=self.hl_columns)

            hl_dictionary[s] = {}
            hl_dictionary[s]['distribution'] = distribution

        emo = self.emotion_calc(emo_df,self.emotion_columns)
        emo1 = self.emotion_calc(emo_df1,self.emotion_columns1)
        select = self.default_select if not select else select
        means = self.avg_graph_separated(data1,num_groups=num_groups,metric=metric,select=select)

        averages = self.aggregate_groups(df1,num_groups,duration=self.duration1,metric=metric)
        averages.index = averages['second']

        return {"high_low":hl_dictionary,"stacked_graph":emo,
                "radar_chart":radar,"range_averages":averages.to_dict(),
                "averages":means,'radar_chart_less_emo':radar1,'stacked_graph_less_emo':emo1}

    def emotion_calc(self,emo_df,columns):
        emo_df = emo_df.groupby(['second', 'emotion']).size()

        emo_df  = pd.pivot_table(pd.DataFrame(emo_df).reset_index(level=['second', 'emotion']),
                              index = "second",columns = "emotion", aggfunc = "sum",fill_value=0)

        emo_df.columns = emo_df.columns.droplevel(0)

        for i in columns:
            if i not in emo_df.columns:
                emo_df[i] = 0

        #emo_df.columns = emo_df.columns.droplevel(0)
        emo_df.reset_index(['second']+list(emo_df.columns))
        emo_df['second'] = emo_df.index
        emo_df.index = range(1,emo_df.shape[0]+1)
        groups =  20 if emo_df.shape[0]>=20 else emo_df.shape[0]
        emo_df = self.aggregate_groups(emo_df, groups,duration=self.duration2,metric="avg")

        emo_df.index = range(1,emo_df.shape[0]+1)

        emo_df['second1'] = emo_df['second']
        del emo_df['second']

        emo_df.index = emo_df.second1
        emo_df.columns = list(emo_df.columns)
        del emo_df['second1']
        emo_df.reset_index()
        emo_df['second'] = emo_df.index
        emo = self.stacked(emo_df,default_columns=columns)
        return emo

    def get_list(self,dict_list,column):
        return map(lambda x: x[column],dict_list)

    def get_age_date(self,dates):
        if isinstance(dates,list):
            return map(lambda x : datetime.date.today() - relativedelta(years=x),dates)
        return datetime.date.today() - relativedelta(years=dates)

    def high_low(self,df,column):
        if df[column] <=25:
            return "low"
        elif df[column] <=50:
            return "medium"
        elif df[column] <=75:
            return "high"
        else:
            return "very high"

    def make_emotion1(self,df):
        if df["YY"] <=1:
            if df["ET"] <=1 :
                return "Bored"
            elif df["ET"] >0  and df["ET"] < 34:
                return "Sad"
            else:
                return "Angry"
        else :
            if df["ET"] <=34 :
                return "Content"
            elif df["ET"] > 34 and df["ET"] < 67:
                return "Happy"
            else :
                return "Excited"

    def make_emotion(self, df):
        if df['YY'] >= .66:
            if df['ET'] >= .25:
                return 4
        elif df['ET'] < .25:
            return 3

        if df['YY'] >= .33 and df['yy'] < .66:
            if df['ET'] >= .50:
                return 3
            elif df['ET'] < .50:
                return 2

        if df['YY'] < .33:
            if df['ET'] >= .75:
                return 2
            elif df['ET'] < .75:
                return 1


    def make_emotion3(self,df):
        if df["YY"] <=1:
            if df["ET"] <=34:
                return ("Sad",df['second'])
            elif df["ET"] >34:
                return ("Tense",df['second'])
        elif df["YY"] >1 and df["YY"] <=51:
            return ("Neutral",df['second'])
        else :
            if df["ET"] <=34:
                return ("Relaxed",df['second'])
            else :
                return ("Happy",df['second'])


    def make_emotion2(self,df):
        if df["YY"] < 50:
            if df["ET"] < 34 :
                return "Sad"
            else:
                return "Tense"
        else :
            if df["ET"] < 34 :
                return "Relaxed"
            else :
                return "Happy"

    def age_filter(self,age,filter):
        if age in self.default_age:
            filter.update(self.default_age[age])

    def pop_age_filter(self,age,filter):
        if age in self.default_age:
            for k in self.default_age[age].keys():
                if k in filter:
                    filter.pop(k)


    def media_filter(self,media_id,filter):
        filter.update({"media_id":media_id})

    def get_media_name(self,media_id):
        media = None
        try:
            media= Media.objects.get(id = media_id)
        except:
            pass
        media_name = media.name if media!=None else ""
        return media_name

    def gender_filter(self,gender,filter):
        if gender=="male":
            filter.update({'gender__in':["male"]})
        else:
            if 'gender__in' in filter:
                filter.pop('gender__in')
            filter.update({'gender':gender})

    def pop_gender(self,gender,filter):
        if gender in filter:
            filter.pop(gender)

    def mediatest_users(self,media_id,users = None):
        filterQ = {}
        if users:
            filterQ.update({"user_tester__user_id__in":users})
        filterQ.update({"media_test__media_id":media_id})
        users = map(lambda x : x[0],MediaTesterMapping.objects.filter(**filterQ).values_list("user_tester__user_id"))
        return list(users)

    def todataframe(self,data,values):
        data = list(data)
        dictionary_to_dataframe = {}
        for c in values:
            dictionary_to_dataframe[c]=self.get_list(data,c)
        return dictionary_to_dataframe

    def default_result(self,select=None):
        select =self.default_select if not select else select
        default_avg= {s:0 for s in select}
        default_range = {s: {} for s in select}
        # default_high_low = {'high_low':{i:[] for i in self.hl_columns}}
        default_high_low = {'high_low': {s: {'distribution':[]} for s in select}}
        self.default_values = {
            "stacked_graph":[],
            "stacked_graph_less_emo": [],
            "graph":{},
            "radar_chart":{ e:0  for e in self.emotion_columns },
            "radar_chart_less_emo": {e: 0 for e in self.emotion_columns1},
            "averages": default_avg
            }

        averages =  {
            #"stacked_graph":[],
            "graph":{},
            #"radar_chart":{},
            # "averages": default_avg
            }
        range_averages = {"range_averages": default_range}
        self.default_values.update(range_averages)
        self.default_values.update(default_high_low)
        return averages

    def convert2Array(self,media_id,age,gender,result,metric = "avg"):
        resultarray = []
        for m in media_id:
            newresult = {}
            newresult.update({"media_id":m})
            newresult.update({"metric":metric})
            newresult['data'] =[]
            temp_result = {}
            #newresult.update({"media_name":self.get_media_name(m)})
            if gender!= "all" and gender!=None:
                for g in gender:

                    temp_result = {}
                    temp_result.update({"gender": g})
                    if age!="all" and age!=None:
                        age_array = []
                        for a in age:
                            temp = {}
                            temp.update({"age": self.age_map[int(a)]})

                            temp.update(result[m][g][a])
                            age_array.append(temp)
                        temp_result.update({"age": age_array})
                    else:
                        # temp_result.update({"age":"all"})
                        temp_result.update({"age":[{"graph":result[m][g][age]["graph"],"age":age}]})

                    newresult['data'] += [temp_result]

            else:
                temp_result.update({"gender": "all"})

                if age != "all" and age != None:
                    age_array = []
                    for a in age:
                        temp = {}
                        temp.update({"age": self.age_map[int(a)]})

                        temp.update(result[m][a])
                        age_array.append(temp)
                    temp_result.update({"age":age_array})
                    newresult['data'] += [temp_result]
                else:
                    # temp_result.update({"age": "all"})
                    temp_result.update({"age":[{"graph":result[m][age]["graph"],"age":age}]})
                newresult['data'] += [temp_result]

            for graphs in ['range_averages','averages','high_low','stacked_graph','stacked_graph_less_emo',
                           'radar_chart_less_emo','radar_chart']:
                if graphs not in result[m]:
                    newresult.update({graphs:self.default_values[graphs]})
                else:
                    newresult.update({graphs:result[m][graphs]})

            newresult.update({'media_name':self.get_media_name(m)})
            resultarray.append(newresult)
        return resultarray


    def post(self,request):
        # separator arguments gender , media_id , age
        request.POST._mutable = True
        info = request.data
        select  = set(self.default_select)
        age = info["age"] if "age" in info else None
        age = [29,49,50] if age == "versus" else (age if age else "all")
        users = info["users"] if "users" in info else None
        gender = info["gender"] if "gender" in info else None
        gender = ["male","female"] if gender == "versus" else ("all" if not gender else gender)
        media_id = info["media_id"] if isinstance(info["media_id"],list) else [info["media_id"]]
        metric = info['metric'] if 'metric' in info else "avg"
        if 'select' in info:
            select.update(info['select'])
        select = list(select)
        # if len(users) == 1:
        filterQ = {}
        values = ["second"]+select
        media_users = {}

        for m in media_id:
            media_users.update({m:self.mediatest_users(m,users)})

        all_m_users = copy.deepcopy(media_users)

        for m in media_id:
            filterQ = {}
            filterQ.update({"user_id__in": media_users[m]})
            media_users[m] = {}
            if gender!= "all" and gender!=None:
                for g in gender:
                    media_users[m][g] = {}
                    self.gender_filter(g, filterQ)
                    if age!="all" and age!=None:
                        for a in age:
                            media_users[m][g][a] = {}
                            self.age_filter(a,filterQ)
                            media_users[m][g][a] = map(lambda x : x[0],UserTester.objects.filter(**filterQ).values_list("user_id"))
                            self.pop_age_filter(a,filterQ)
                    else:
                        media_users[m][g] = map(lambda x : x[0],UserTester.objects.filter(**filterQ).values_list("user_id"))
                    # self.pop_gender(g, filterQ)
            else:
                if age != "all" and age != None:
                    for a in age:
                        self.age_filter(a, filterQ)
                        media_users[m][a] = map(lambda x : x[0],UserTester.objects.filter(**filterQ).values_list("user_id"))
                        self.pop_age_filter(a, filterQ)
                else:
                    media_users[m] = map(lambda x : x[0],UserTester.objects.filter(**filterQ).values_list("user_id"))
        result = {}
        flag = 0
        all_users = users
        for m in media_id:
            result[m] = {}
            allusers_data = ContentAnalysis.objects.filter(media_id=m).values(*values)
            if all_users:
                filtered_users_data = ContentAnalysis.objects.filter(media_id=m,
                                                  user_id__in=all_users).values(*values)
            else:
                filtered_users_data = ContentAnalysis.objects.filter(media_id=m).values(*values)

            if gender!= "all" and gender!=None:
                for g in gender:
                    result[m][g] = {}
                    if age!="all" and age!=None:
                        for a in age:
                            users = media_users[m][g][a]
                            if not users:
                                result[m][g][a] = self.default_result()
                                continue
                            data = ContentAnalysis.objects.filter(media_id=m,
                                    user_id__in=users).values(*values)
                            if not list(data) or not data[0]['second']!=None:
                                result[m][g][a] = self.default_result()
                                continue
                            result[m][g][a] = self.aggregatorAcrossSecondGroups(self.todataframe(data,values),
                                                                                self.todataframe(filtered_users_data,values),
                                                                                self.todataframe(allusers_data, values),
                                                                                metric=metric,select=select,graph=1)
                    else:
                        users = media_users[m][g]
                        if not users:
                            result[m][g][age] = self.default_result()
                            continue
                        data = ContentAnalysis.objects.filter(media_id=m,
                                                              user_id__in=users).values(*values)

                        if not list(data) or not data[0]['second']!=None:
                            result[m][g][age] = self.default_result()
                            continue

                        result[m][g][age] = self.aggregatorAcrossSecondGroups(self.todataframe(data,values),
                                                                         self.todataframe(filtered_users_data, values),
                                                                         self.todataframe(allusers_data, values),
                                                                         metric=metric,select=select,graph = 1)

            else:
                if age != "all" and age != None:
                    for a in age:
                        users = media_users[m][a]
                        if not users:
                            result[m][a] = self.default_result()
                            continue
                        data = ContentAnalysis.objects.filter(media_id=m,
                                                              user_id__in=users).values(*values)
                        if not list(data) or not data[0]['second']!=None:
                            result[m][a] = self.default_result()
                            continue

                        result[m][a] = self.aggregatorAcrossSecondGroups(self.todataframe(data,values),
                                                                         self.todataframe(filtered_users_data, values),
                                                                         self.todataframe(allusers_data, values),
                                                                         metric=metric,select=select,graph = 1)

                else:
                    users = media_users[m]
                    if not users:
                        result[m][age] = self.default_result()
                        continue
                    data = ContentAnalysis.objects.filter(media_id=m,
                                                          user_id__in=users).values(*values)

                    if not list(data) or not data[0]['second']!=None:
                        result[m][age] = self.default_result()
                        continue

                    result[m][age] = self.aggregatorAcrossSecondGroups(self.todataframe(data,values),
                                                                  self.todataframe(filtered_users_data, values),
                                                                  self.todataframe(allusers_data, values),
                                                                  metric=metric,select=select,graph = 1)

            data = ContentAnalysis.objects.filter(media_id=m,
                                                      user_id__in=all_m_users[m]).values(*values)

            data = self.aggregatorAcrossSecondGroups(self.todataframe(data, values),
                                                     self.todataframe(filtered_users_data, values),
                                                     self.todataframe(allusers_data, values),
                                              metric=metric, select=select, graph=0)

            result[m].update(data)
        result = self.convert2Array(media_id,age,gender,result)
        return Response(result)


class AvgGraph(APIView):

    def __init__(self):
        self.default_age = {29: {"birth_date__lt": self.get_age_date(29)},
                            50: {"birth_date__gt": self.get_age_date(50)},
                            49: {"birth_date__range": self.get_age_date([50,29])}}
        self.age_map = {29:"0-29", 49:"29-49" ,50 : ">50"}
        #self.emotion_columns = ["Bored","Angry","Sad","Excited","Happy","Content"]
        self.emotion_columns = ["Tense", "Relaxed", "Sad", "Neutral", "Happy"]
        self.metric_map = {"complexity":'ME',"arousal":'ET',"impact":"AP","attention":"attention",'emotional-spectrum':'emotional-spectrum'}
        self.reverse_metric_map = {'ME':"complexity",  'ET':"arousal",  "AP":"impact",  "attention":"attention"}
        self.default_select = [ "ET", "YY"]
        self.hl_columns = ['high','low','medium','very high']
        self.graphs = ['emotional_spectrum','raw_data','overall_average']

    def emo_total(self,df,columns):
        total = 0
        for i in columns:
            total+=df[i]
        return total

    def roundingVals_toTwoDeci(self,y):
        for k, v in y.items():
            if isinstance(v,dict):
                v=self.roundingVals_toTwoDeci(v)
            else:
                v = round(v, 2)
                y[k] = v


    def stacked(self,emo,metric="avg",default_columns=None):

        columns = list(emo.columns)
        emo=emo.reset_index()
        emo["total"] = emo.apply(lambda x : self.emo_total(x,default_columns),axis=1)

        for i in default_columns:
            emo[i]  = (emo[i]*100)/emo["total"]

        columns = ["second"] + columns + ["total"]

        emo.columns = columns
        emo.second = emo.second.round()
        del emo["total"]
        emo = emo.fillna(0)
        emo = emo.T.to_dict()
        emo = [emo[k] for k in emo.keys()]
        return emo

    def aggregate_groups(self,df,num_groups,metric="avg",duration = None):

        duration = self.duration if not duration else duration
        gap = float(duration)/num_groups

        if gap> 1:
            ranges  = map(lambda x : int(x),[(gap * i) for i in range(num_groups + 1)])
        else:
            ranges = range(duration+1)

        averages = {}
        if metric =="avg":
            averages = df.groupby(pd.cut(df['second'],ranges)).mean()
        else:
            averages = df.groupby(pd.cut(df['second'], ranges)).std()
        averages['second'] = map(lambda x : eval(str(x).replace(')',']').replace('(','['))[1],list(averages.index))

        return averages

    def transform2rickshaw(self,dictionary,select):
        transform = {}
        for c in select:
            transform[c]=[{'x':k,'y': dictionary[c][k],
            'vws':dictionary['vws'][k],} for k in dictionary[c].keys()]
        return transform

    def general_graphs(self,df,metric="avg",select=None):
        if metric == "avg":
            mean = df.groupby('second').mean()
        else:
            mean = df.groupby('second').std()

        mean['vws'] = df.groupby('second').size()
        mean['x'] = mean.index

        selected = mean.to_dict()

        select = self.default_select if not select else select
        return self.transform2rickshaw(selected,select)

    def map_metrics(self,data):
        for k in data:
            if isinstance(k,dict):
                k=self.map_metrics(k)
            if k in self.reverse_metric_map:
                data[self.reverse_metric_map[k].title()] = data.pop(k)
        return data

    def avg_graph_separated(self,data,num_groups= 10,metric="avg",select=None):
        df = pd.DataFrame(data).fillna(0)
        self.duration1 = max(df['second'])
        select = self.default_select if not select else select
        means = {}
        for c in select:
            if metric=="avg":
                means[c]=round(np.mean(df[c]),2)
            else:
                means[c] = round(np.std(df[c]),2)
        return means

    def round_dataframe(self,df):
        tmp = df.select_dtypes(include=[np.number])
        df.loc[:, tmp.columns] = np.around(tmp,decimals=2)
        return df

    #def aggregatorAcrossSecondGroups(self,data,data1,data2,num_groups= 10,metric="avg",select=None,graph=1):
    def aggregatorAcrossSecondGroups(self,data,num_groups= 10,metric="avg",select=None):

        df = pd.DataFrame(data).fillna(0)
        if not df.shape[0]:
            return self.default_result(select)
        self.duration = max(df['second'])

        if num_groups=="max":
            num_groups0 = df.shape[0]

        select = select if select else self.default_select

        df1 = df
        self.duration1 = self.duration
        df2 = df
        self.duration2 = self.duration

        if self.emotional_spectrum:
            emo_df = pd.DataFrame(df2.apply(lambda x: self.make_emotion3(x), axis=1), columns=["emotion"])

            total_emotion =  emo_df.groupby('emotion').size().sum()

            emo_df["second"] = df2["second"]

            emo_df = emo_df.groupby(['second', 'emotion']).size()
            emo_df  = pd.pivot_table(pd.DataFrame(emo_df).reset_index(level=['second', 'emotion']),
                                  index = "second",columns = "emotion", aggfunc = "count",fill_value=0)
            emo_df.columns = emo_df.columns.droplevel(0)

            for i in self.emotion_columns:
                if i not in emo_df.columns:
                    emo_df[i] = 0

            #emo_df.columns = emo_df.columns.droplevel(0)
            emo_df.reset_index(['second']+list(emo_df.columns))
            emo_df['second'] = emo_df.index
            emo_df.index = range(1,emo_df.shape[0]+1)
            groups =  20 if emo_df.shape[0]>=20 else emo_df.shape[0]

            if num_groups == "max":
                num_groups2 = emo_df.shape[0]
            else:
                num_groups2 = groups

            emo_df = self.aggregate_groups(emo_df, num_groups2,duration=self.duration2,metric="avg")

            emo_df.index = range(1,emo_df.shape[0]+1)

            emo_df['second1'] = emo_df['second']
            del emo_df['second']
            select = self.default_select if not select else select
            emo_df.index = emo_df.second1
            emo_df.columns = list(emo_df.columns)
            del emo_df['second1']
            emo_df.reset_index()
            emo_df['second'] = emo_df.index
            emo = self.stacked(emo_df,default_columns=self.emotion_columns)
            for e in emo:
                e = self.roundingVals_toTwoDeci(e)
            return {"emotional_spectrum":emo}

        if num_groups=="max":
            num_groups1=df1.shape[0]
        else:
            num_groups1=num_groups

        means = self.avg_graph_separated(data,num_groups=num_groups1,metric=metric,select=[self.metric_map[self.selection]])
        averages = self.aggregate_groups(df1,num_groups1,duration=self.duration1,metric=metric)
        averages.index = averages['second']
        averages['second'] = averages['second'].astype(int)
        raw_data = averages.to_dict()
        self.roundingVals_toTwoDeci(raw_data)

        for k in raw_data.keys():
            if k != self.metric_map[self.selection]:
                raw_data.pop(k)

        return {
            "raw_data": self.map_metrics(raw_data),
            "overall_average":self.map_metrics(means)}

    def get_list(self,dict_list,column):
        return map(lambda x: x[column],dict_list)

    def get_age_date(self,dates):
        if isinstance(dates,list):
            return map(lambda x : datetime.date.today() - relativedelta(years=x),dates)
        return datetime.date.today() - relativedelta(years=dates)

    def high_low(self,df,column):
        if df[column] <=25:
            return "low"
        elif df[column] <=50:
            return "medium"
        elif df[column] <=75:
            return "high"
        else:
            return "very high"


    def make_emotion3(self,df):
        if df["YY"] <=1:
            if df["ET"] <=34 :
                return "Sad"
            elif df["ET"] >34 :
                return "Tense"
        elif df["YY"] >1 and df["YY"] <=51 :
            return "Neutral"
        else :
            if df["ET"] <=34 :
                return "Relaxed"
            else :
                return "Happy"


    def make_emotion1(self,df):
        if df["YY"] <=1:
            if df["ET"] <=1 :
                return "Bored"
            elif df["ET"] >0  and df["ET"] < 34:
                return "Sad"
            else:
                return "Angry"
        else :
            if df["ET"] <=34 :
                return "Content"
            elif df["ET"] > 34 and df["ET"] < 67:
                return "Happy"
            else :
                return "Excited"

    def make_emotion(self, df):
        if df['YY'] >= .66:
            if df['ET'] >= .25:
                return 4
        elif df['ET'] < .25:
            return 3

        if df['YY'] >= .33 and df['yy'] < .66:
            if df['ET'] >= .50:
                return 3
            elif df['ET'] < .50:
                return 2

        if df['YY'] < .33:
            if df['ET'] >= .75:
                return 2
            elif df['ET'] < .75:
                return 1

    def make_emotion2(self,df):
        if df["YY"] < 50:
            if df["ET"] < 34 :
                return "Sad"
            else:
                return "Tense"
        else :
            if df["ET"] < 34 :
                return "Relaxed"
            else :
                return "Happy"

    def age_filter(self,age,filter):
        if age in self.default_age:
            filter.update(self.default_age[age])

    def pop_age_filter(self,age,filter):
        if age in self.default_age:
            for k in self.default_age[age].keys():
                if k in filter:
                    filter.pop(k)


    def media_filter(self,media_id,filter):
        filter.update({"media_id":media_id})

    def get_media_name(self,media_id):
        media = None
        try:
            media= Media.objects.get(id = media_id)
        except:
            pass
        media_name = media.name if media!=None else ""
        return media_name

    def gender_filter(self,gender,filter):
        if gender=="male":
            filter.update({'gender__in':["male"]})
        else:
            if 'gender__in' in filter:
                filter.pop('gender__in')
            filter.update({'gender':gender})

    def pop_gender(self,gender,filter):
        if gender in filter:
            filter.pop(gender)

    def mediatest_users(self,media_id,users = None):
        filterQ = {}
        if users:
            filterQ.update({"user_tester__user_id__in":users})
        filterQ.update({"media_test__media_id":media_id})
        users = map(lambda x : x[0],MediaTesterMapping.objects.filter(**filterQ).values_list("user_tester__user_id"))
        return list(users)

    def todataframe(self,data,values):
        data = list(data)
        dictionary_to_dataframe = {}
        for c in values:
            dictionary_to_dataframe[c]=self.get_list(data,c)
        return dictionary_to_dataframe

    def default_result(self,select=None):
        select =self.default_select if not select else select
        select = [self.selection]
        default_avg= {s:0 for s in select}
        default_range = {s: {} for s in select}
        # default_high_low = {'high_low':{i:[] for i in self.hl_columns}}
        # default_high_low = {'high_low': {s: {'distribution':[]} for s in select}}
        if self.emotional_spectrum:
            self.default_values = {
                "emotional_spectrum":[],
                }
        else:
            self.default_values = {
                "overall_average": default_avg
                }
            range_averages = {"raw_data": default_range}
            self.default_values.update(range_averages)


    def convert2Array(self,media_id,age,gender,result,metric = "avg"):
        resultarray = []
        for m in media_id:
            newresult = {}
            newresult.update({"media_id":m})
            newresult.update({"metric":metric})
            # newresult['data'] =[]
            temp_result = {}
            #newresult.update({"media_name":self.get_media_name(m)})
            if gender!= "all" and gender!=None:
                for g in gender:

                    temp_result = {}
                    temp_result.update({"gender": g})
                    if age!="all" and age!=None:
                        age_array = []
                        for a in age:
                            temp = {}
                            temp.update({"age": self.age_map[int(a)]})

                            temp.update(result[m][g][a])
                            age_array.append(temp)
                        temp_result.update({"age": age_array})

                    # newresult['data'] += [temp_result]

            else:
                temp_result.update({"gender": "all"})

                if age != "all" and age != None:
                    age_array = []
                    for a in age:
                        temp = {}
                        temp.update({"age": self.age_map[int(a)]})

                        temp.update(result[m][a])
                        age_array.append(temp)
                    temp_result.update({"age":age_array})
                #     newresult['data'] += [temp_result]
                # newresult['data'] += [temp_result]

            for graphs in self.graphs:
                # if graphs not in result[m]:
                #     newresult.update({graphs:self.default_values[graphs]})
                # else:
                if graphs in result[m]:
                    newresult.update({graphs:result[m][graphs]})

            newresult.update({'media_name':self.get_media_name(m)})
            resultarray.append(newresult)
        return resultarray

    def missing_response(self,api_key,media_id,selection):
        if not api_key:
            return "Please provide api_key"
        elif not media_id:
            return "Please provide media_id"
        elif not selection:
            return "Please provide metric"
        else:
            return False

    def get(self,request,api_key=None,media_id=None,selection=None):
        if not api_key:
            api_key=request.GET.get('api_key')
            media_id = request.GET.get('media_id')
            selection = request.GET.get('metric')
            missing_response = self.missing_response(api_key,media_id,selection)
            if missing_response:
                return Response({"status":missing_response})
        token = api_key
        selection = selection.strip().lower()
        self.selection = selection
        if self.selection not in self.metric_map:
            return Response({"status":"Invalid metric. Please select from any of these, "+",".join(self.metric_map.keys())})
        admin_id = ApiWrapper().token_valid_check(token)
        if not admin_id:
            return Response({"status": "Api key not found"})
        try:
            media = Media.objects.get(project__admin = admin_id, id = media_id)
        except:
            return Response({"status":"Invalid or Unauthorized Access to specified media"})
        select  = set(self.default_select)
        age = "all" # info["age"] if "age" in info else None
        #age = [29,49,50] if age == "versus" else age if age else "all"
        users = None # info["users"] if "users" in info else None
        gender = None #info["gender"] if "gender" in info else None
        gender= "all" # ["male","female"] if gender == "versus" else ("all" if not gender else gender)
        media_id = [media_id]#info["media_id"] if isinstance(info["media_id"],list) else [info["media_id"]]
        metric = "avg" #info['metric'] if 'metric' in info else "avg"
        # if 'select' in info:
        #     select.update(info['select'])
        select = list(select)
        # if len(users) == 1:
        filterQ = {}
        self.emotional_spectrum = False
        if selection == 'emotional-spectrum':
            values = ["second"]+ select
            self.emotional_spectrum = True
        else:
            values = ["second"] + [self.metric_map[selection]]
        media_users = {}

        for m in media_id:
            media_users.update({m:self.mediatest_users(m,users)})

        all_m_users = copy.deepcopy(media_users)

        for m in media_id:
            filterQ = {}
            filterQ.update({"user_id__in": media_users[m]})
            media_users[m] = {}
            if gender!= "all" and gender!=None:
                for g in gender:
                    media_users[m][g] = {}
                    self.gender_filter(g, filterQ)
                    if age!="all" and age!=None:
                        for a in age:
                            media_users[m][g][a] = {}
                            self.age_filter(a,filterQ)
                            media_users[m][g][a] = map(lambda x : x[0],UserTester.objects.filter(**filterQ).values_list("user_id"))
                            self.pop_age_filter(a,filterQ)
                    else:
                        media_users[m][g] = map(lambda x : x[0],UserTester.objects.filter(**filterQ).values_list("user_id"))
            else:
                if age != "all" and age != None:
                    for a in age:
                        self.age_filter(a, filterQ)
                        media_users[m][a] = map(lambda x : x[0],UserTester.objects.filter(**filterQ).values_list("user_id"))
                        self.pop_age_filter(a, filterQ)
                else:
                    media_users[m] = map(lambda x : x[0],UserTester.objects.filter(**filterQ).values_list("user_id"))
        result = {}
        flag = 0
        all_users = users
        for m in media_id:
            result[m] = {}
            allusers_data = ContentAnalysis.objects.filter(media_id=m).values(*values)

            if gender!= "all" and gender!=None:
                for g in gender:
                    result[m][g] = {}
                    if age!="all" and age!=None:
                        for a in age:
                            users = media_users[m][g][a]
                            if not users:
                                result[m][g][a] = self.default_result()
                                continue
                            data = ContentAnalysis.objects.filter(media_id=m,
                                    user_id__in=users).values(*values)
                            if not list(data) or not data[0]['second']!=None:
                                result[m][g][a] = self.default_result()
                                continue

                            result[m][g][a] = self.aggregatorAcrossSecondGroups(self.todataframe(data,values),
                                                                                metric=metric,select=select,num_groups="max")
                    else:
                        users = media_users[m][g]
                        if not users:
                            result[m][g][age] = self.default_result()
                            continue
                        data = ContentAnalysis.objects.filter(media_id=m,
                                                              user_id__in=users).values(*values)


                        if not list(data) or not data[0]['second']!=None:

                            result[m][g][age] = self.default_result()
                            continue

                        result[m][g][age] = self.aggregatorAcrossSecondGroups(self.todataframe(data,values),
                                                                         metric=metric,select=select,num_groups="max")

            else:
                if age != "all" and age != None:
                    for a in age:
                        users = media_users[m][a]
                        if not users:
                            result[m][a] = self.default_result()
                            continue
                        data = ContentAnalysis.objects.filter(media_id=m,
                                                              user_id__in=users).values(*values)
                        if not list(data) or not data[0]['second']!=None:
                            result[m][a] = self.default_result()
                            continue

                        result[m][a] = self.aggregatorAcrossSecondGroups(self.todataframe(data,values),
                                                    metric=metric,select=select,num_groups="max")

                else:
                    users = media_users[m]

                    if not users:
                        result[m][age] = self.default_result()
                        continue
                    data = ContentAnalysis.objects.filter(media_id=m,
                                                          user_id__in=users).values(*values)

                    if not list(data) or not data[0]['second']!=None:
                        result[m][age] = self.default_result()
                        continue

                    result[m][age] = self.aggregatorAcrossSecondGroups(self.todataframe(data,values),
                                                    metric=metric,select=select,num_groups="max")

            data = ContentAnalysis.objects.filter(media_id=m,
                                                      user_id__in=all_m_users[m]).values(*values)

            data = self.aggregatorAcrossSecondGroups(self.todataframe(data, values),
                                                    metric=metric, select=select,num_groups="max")

            result[m].update(data)

        result = self.convert2Array(media_id,age,gender,result)[0]
        result.pop("media_id")
        result.update({"type":"media","id": media_id[0],"attributes":{"name":result.pop("media_name")}})
        result.pop("metric")
        result = {"data":result}
        return Response(result)


class ApiWrapper(APIView):
    def token_valid_check(self,token):
        try:
            user = AdminApiAuth.objects.get(api_key=token)
            return user.admin_id
        except:
            return False

    def post(self,request):
        admin_id = request.data['admin_id']
        token = request.data['api_key']
        api = request.data['api']
        payload = request.data['payload']
        response = self.token_valid_check(token)
        if not response:
            return Response({"status":"Admin Id and token don't match"})
        response=requests.post(settings.API_ENDPOINT+api,json=payload,auth=HTTPBasicAuth('apiuser', 'entropika123'))
        return Response(json.loads(response.content))


class Toggle_Deactivate_User(APIView):
    def post(self,request):
        request.POST._mutable = True
        info = request.data
        user_id = info['user_id']
        try:
            user = User.objects.get(id = user_id)
        except:
            return Response({"status":"User not found"})
        try:
            ma = MailActivation.objects.get(user=user.id)
            ma.email_verified=True
        except:
            pass
        user.is_active = not user.is_active
        uas = UserActiveSerializer(user,data={"is_active":user.is_active},partial=True)
        user.save()
        if uas.is_valid():
            uas.save()

        return Response({"status":"Done"})


class GetDeactivatedStatus(APIView):
    def post(self,request):
        request.POST._mutable = True
        try :
            user = Admin.objects.get(user_id=request.data['user_id'])
        except:
            return Response({"status":"Admin not found"})
        testers=list(UserTester.objects.filter(admin_id = user.id).values('user_id','email','user__is_active'))
        for v in testers:
            v['is_active'] = v.pop('user__is_active')
        return Response(testers)


class VideoUpload(TemplateView):
    template_name = 'dashboard/video_play.html'
    def upload(self, request, login_id):
        return render_to_response(self.template_name, {'login_id': login_id})
        return render_to_response(self.template_name, {'login_id': login_id})


class LoginView(TemplateView):
    template_name = 'dashboard/login.html'


def get_admin_id(user_id):
    admin_id = -1

    admin_id = map(lambda x: x[0], list(Admin.objects.filter(user=user_id).values_list('user_id')))
    if admin_id:
        admin_id = admin_id[0]
    else:
        admin_id = map(lambda x: x[0], list(UserTester.objects.filter(user=user_id).values_list('admin__user_id')))
        admin_id = admin_id[0] if admin_id else -1
    return admin_id


class LoginCreateView(APIView):
    # permission_classes = (IsAdminUser,)
    # authentication_classes = (QuietBasicAuthentication,)
    def post(self, request):
        request.POST._mutable = True
        version,device_type = None,None
        if "version" in request.data and "device_type" in request.data:
            version, device_type = request.data["version"], request.data["device_type"]

        valid, email_login, username_login = 0, 0, 0
        try:
            email = User.objects.get(email=request.data["email"],is_active=True)
            password = request.data['password']
            valid = email.check_password(password)
            email_login = 1
            user_id = email.id
        except:
            try:
                username = User.objects.get(username=request.data["email"],is_active=True)
                password = request.data['password']
                valid = username.check_password(password)
                username_login = 1
                user_id = username.id
            except:
                return Response({"status": "Invalid Username Password"},
                                status=status.HTTP_200_OK)
            pass
        if valid:
            lastlogin = datetime.datetime.now()
            if username_login:
                serializer = LoginSerializer(data={'username': request.data['email'],
                                                   "last_login": lastlogin, 'email': username.email,
                                                   "version": version, "device_type": device_type})
            elif email_login:
                serializer = LoginSerializer(data={'username': email.username, "last_login": lastlogin,
                                                   'email': request.data['email'],
                                                   "version": version, "device_type": device_type})

            if serializer.is_valid():
                serializer.save()
            status_ = {"status": "Login Successful"}
            status_.update(serializer.data)
            status_.update({"user_id": user_id})
            if "id" in status_:
                status_.pop('id')
            admin_id = get_admin_id(user_id)
            status_.update({"user_id": user_id, "admin_id": admin_id})
            return Response(status_, status=status.HTTP_200_OK)
        return Response({"status": "Invalid email or password"}, status=status.HTTP_200_OK)


# class LoginCreateView(APIView):
#     permission_classes = (IsAdminUser,)
#     authentication_classes = (QuietBasicAuthentication,)

#     def post(self,request):
#         user = Login.objects.filter(email=request.data["email"])

#         if user:
#             return Response({"id":user[0].id,"name":user[0].name,"email":user[0].email})

#         serializer = LoginSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()

#             return Response(serializer.data)
#         else:
#             return Response(
#                 serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class getActivityTypeList(APIView):
    def get(self,request):
        return Response(ActivityType.objects.filter().values())


class GetEmailId(APIView):
    def post(self, request):
        try:
            mail_activation = MailActivation.objects.get(activation_key=request.data['variable'])
        except:
            return Response({"status": "activation key not found"})
        try:
            user = User.objects.get(pk=mail_activation.user_id)
        except:
            return Response({"status": "User not found"})
        tzinfo = mail_activation.link_date.tzinfo
        is_expired = (mail_activation.link_date + timedelta(days=1)) < datetime.datetime.now(
            tzinfo) or mail_activation.is_expired
        if is_expired and not mail_activation.is_expired:
            mail_activation.is_expired = True
            mail_activation.save()
        return Response({"email": user.email, "user_id": user.id, "email_verified": mail_activation.email_verified
                            , "is_expired": is_expired})


class MediaBulkUpload(APIView):
    def post(self, request):
        path = os.path.join(settings.PROJECT_DIR + "/static/media/")
        if not os.path.isdir(path):
            os.mkdir(path)
        media_id = str(request.data['media_id'])
        path = os.path.join(settings.PROJECT_DIR + "/static/media/" + media_id)
        if not os.path.isdir(path):
            os.mkdir(path)

        filename = os.path.join(settings.PROJECT_DIR + "/static/media/", media_id,
                                str(request.FILES['file'].name))
        # request.data.pop("media_id")
        media = open(filename, "wb")
        media.write(request.FILES["file"].read())
        media.close()
        zip_ref = zipfile.ZipFile(filename)
        zip_ref.extractall(path + "/")
        zip_ref.close()
        os.remove(filename)
        return Response({"status": "success"})


class CreateTester(APIView):
    def create_user(self, email, date_now):
        # name = email.split("@")[0]
        return UserSerializer(data={'name': email, 'created_date': date_now,
                                    'role': "usertester", 'username': email,
                                    'email': email, 'password': "affectlab"})

    def send_mail(self, request, email):
        SendMail().send_email(request, email)

    def common_function(self,info,pseudo_user=False,password=None,name=None):
        response = ""
        dob = info['date_of_birth'] if 'date_of_birth' in info else None
        gender = info['gender'] if 'gender' in info else None
        user_ids = []
        if isinstance(info, QueryDict):
            info = dict(info.iterlists())
            emails = info['emails']
        else:
            emails = info['emails']
        try:
            admin_id = info['admin_id'][0] if isinstance(info['admin_id'], list) else info['admin_id']
            admin_id = get_admin_id(admin_id)
            admin = Admin.objects.get(user_id=int(admin_id))
        except:
            return Response({"status":"Failed, Admin Doesn't exist"}, status=status.HTTP_200_OK)
        for email in emails:
            email = "_".join(email.split())
            user = map(lambda x: x[0], User.objects.filter(email=email).values_list('id'))
            if not user:
                date_now = datetime.datetime.now()
                user = self.create_user(email, date_now)
                if not user.is_valid():
                    continue
                data = {"email": email,
                        "username": email,
                        "password": password if pseudo_user and password else email[::-1].lower(),
                        "name": name if name else email,
                        "created_date": date_now,
                        "role": "usertester",
                        "date_of_birth": dob if dob else datetime.date.today(),
                        "gender": gender if  gender else None,
                        "admin": admin_id}
                user = user.create_tester(data, admin)
                user_id = user.id
                user_ids.append(user_id)
                if not pseudo_user:
                    try:
                        # custom_message = "Thanks for sending us request, the requested url is %sSignup/%s " %\
                        #                (settings.API_HOSTNAME,user_id)
                        t = threading.Thread(target=self.send_mail, args=[request, email])
                        t.setDaemon(False)
                        t.start()
                        if response:
                            response += "User Tester %s Created Successfully, " % (email)

                            # self.send_mail(request, email)
                            # SendMail().send_mail({'type': 'ios', 'email': email},custom_message)
                    except:
                        response += email + " ," if response else "Email Not Sent for " + email + " ,"
                else:
                    user = User.objects.get(email=email)
                    user.is_active = True
                    activation_key = generate_activation_key(email)
                    data = {"user": user.id,
                            "email_verified": False, "activation_key": str(activation_key),
                            "link_date": datetime.datetime.now(), "is_expired": False}
                    ids = map(lambda x: x[0], MailActivation.objects.filter(user=user.id).values_list('id'))
                    for i in ids:
                        ma = MailActivation.objects.get(pk=i)
                        ma.is_expired = True
                        ma.save()
                    user_profile = MailActivationSerializer(data=data)
                    is_valid = user_profile.is_valid()
                    if is_valid:
                        user_profile.save()
                    ma = MailActivation.objects.get(user_id=user.id, is_expired=False)
                    ma.email_verified = True
                    ma.is_expired = True
                    ma.save()
                    user.save()
            else:
                user_id = user[0]

                response += "User Tester %s already exists , "%(email)
        response = {"status": response.strip(" ,")+"."} if response else {"status": "Created"}
        response.update({'user_ids':user_ids})
        return response

    def post(self, request):
        info = request.data
        return Response(self.common_function(info))

class CreatePseudoTester(CreateTester):

    def post(self,request):
        number = request.data['number']
        tester_prefix = request.data['tester_prefix']
        password = request.data['password']
        start = request.data['start'] if 'start' in request.data else 1
        admin_id = request.data['admin_id']
        info ={}
        info['emails'] = [tester_prefix+str(i) for i in range(int(start),int(number)+int(start))]
        info['admin_id'] = admin_id

        return Response(self.common_function(info,password=password,pseudo_user=True))

class SignUpPassCode(CreateTester):

    def post(self,request):
        try:
            mpc = MediaPassCode.objects.get(passcode = request.data['passcode'])
        except:
            return  Response({'status':'passcode not found'})
        media = mpc.media
        media_name  =media.name
        admin_id = media.project.admin.user_id
        username = media_name+'_'+''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(16))
        while User.objects.filter(username = username).values_list('id',flat=True):
            username = media_name+'_'+''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(16))
        password = username
        info ={}
        info['emails'] = [username]
        info['admin_id'] = admin_id
        info['name'] = request.data['name']
        info['date_of_birth'] = request.data['date_of_birth'] if 'date_of_birth' in request.data else None
        info['gender'] = request.data['gender'] if 'gender' in request.data else None
        result = self.common_function(info,password=password,pseudo_user=True,name=info['name'])
        result['user_id'] = result.pop('user_ids')
        result['user_id'] = result['user_id'][0] if len(result['user_id']) >= 1 else None
        return Response(result)


class UpdateTester(APIView):
    def post(self, request):
        info = request.data
        if isinstance(info, QueryDict):
            info = dict(info.iteritems())
        email = info['email']

        try:
            user = User.objects.get(email=email)
            user.is_active = True
            usertester = UserTester.objects.get(email=email)
        except:
            return Response({"status": "User or User Tester not found"}, status=status.HTTP_200_OK)
        if 'username' in info:
            user.username = info['username']
        if 'birthdate' in info:
            usertester.birth_date = parser.parse(info['birthdate'],dayfirst=True)
        if 'gender' in info:
            usertester.gender = info['gender']
        if 'name' in info:
            usertester.name = info['name']
        if 'password' in info:
            user.set_password(info['password'])
        try:
            ma = MailActivation.objects.get(user_id=user.id, is_expired=False)
            ma.email_verified = True
            ma.is_expired = True
            ma.save()
        except:
            return Response({"status": "unable to verify email"})
        user.save()
        usertester.save()

        return Response({"status": "success"})


class GetMailActivationUser(APIView):
    def post(self, request):
        return Response(MailActivation.objects.filter(user_id=request.data['user_id'], is_expired=False).values())


class CheckUserAvailable(APIView):
    def post(self, request):
        if User.objects.filter(username=request.data['username']).count() > 0:
            return Response({"status": "username unavailable"})
        return Response({"status": "username available"})

class Graphs1(APIView):
    def __init__(self):
        # default
        self.emotion_sums = {'bored':0 , "disappointed" :0 , "alarmed":0,"happy":0}
        self.default_select = ["second", "attention", "meditation", "BP", "ET", "YF", "Blink", "YY", "AL",
                               "AP", "ME", "CR", "CP","AL_type", "AL_BCQ_valid", "F2_degree", "F2_progress_level",
                               "CR_type", "CR_BCQ_valid","ME2_rate", "ME2_total", "ME2_changing_rate",
                               "CP_type", "CP_BCQ_valid", "BP_theta_power", "BP_alpha_power", "BP_beta_power",
                               "BP_gamma_power", "YF_min", "YF_max", "YF_diff", "ME_min", "ME_max", "ME_diff"
                               ]
        self.default_metrics = ["avg", "var"]
        #self.default_age1 = [{"age__lte": 29}, {"age__gte": 50}, {"age__range": [30, 49]}]
        self.default_age1 = [{"birth_date__lte": self.get_age_date(29)},
                             {"birth_date__gte": self.get_age_date(50)}, {"birth_date__range": self.get_age_date([30, 49])}]
        self.default_age = [{"op": "lte", "params": 29}, {"op": "gte", "params": 50},
                            {"op": "range", "params": (30, 49)}]
        self.range = {"attention": [0, 100], "meditation": [0, 100], "ET": [1, 4], "BP": [-30, 30], "YY": [-1, 1],
                      "YF": [-100, 100],
                      "Blink": [0, 150], "AP": [1, 4], "ME": [-100, 100], "CR": [-1, 1], "CP": [-1, 1], "AL": [-1, 1]}

    def make_emotion1(self,df):
        if df['yy'] == 1:
            if df['et'] in ([2, 3, 4]):
                return 'happy'
            elif df['et'] == 1:
                return 'alarmed'
        elif df['yy'] == 0:
            if df['et'] in ([3, 4]):
                return 'alarmed'
            elif df['et'] in ([1, 2]):
                return 'bored'
        elif df['yy'] == -1:
            if df['et'] == 4:
                return 'bored'
            elif df['et'] in ([1, 2, 3]):
                return 'disappointed'
        return 'undefined'

    def make_emotion(self,df):
        if df['yy'] == 100:
            if df['et'] > 50:
                return 'happy'
            elif df['et'] >= 25 and df['et'] <=50:
                return 'alarmed'
        elif df['yy'] == 50:
            if df['et'] >=50:
                return 'alarmed'
            elif df['et'] >=25 and df['et'] <=50:
                return 'bored'
        elif df['yy'] == 0:
            if df['et'] >= 75:
                return 'bored'
            elif df['et'] >=25 and df['et'] <= 75:
                return 'disappointed'
        return 'undefined'

    def get_age_date(self,dates):
        if isinstance(dates,list) or isinstance(dates,tuple):
            return map(lambda x : datetime.date.today() - relativedelta(years=x),dates)
        return datetime.date.today() - relativedelta(years=dates)

    def build_args(self, gender, age):
        args = {"gender": gender}
        res = []

        if any(map(lambda x: x["op"] == "all", age)):
            if "all" == gender:
                return [1]
            else:
                return [args]
        if age:
            for i in age:

                i["params"] = tuple(i["params"]) if i["op"] == "range" else i["params"]
                i = dict({'{0}__{1}'.format("birth_date", i["op"]): self.get_age_date(i['params'])})  # .update(args)

                if not "all" == gender:
                    i.update({"gender": gender})
                res.append(i)
        else:
            for i in self.default_age:
                i["params"] = tuple(i["params"]) if i["op"] == "range" else i["params"]
                i = dict({'{0}__{1}'.format("birth_date", i["op"]): self.get_age_date(i['params'])})  # .update(args)
                if not "all" == gender:
                    i.update({"gender": gender})
                res.append(i)
        return res

    def get_agg(self, gender, age, filter_query, select, metric, media_id):
        result = []
        emotion_graph = []
        for i in self.build_args(gender, age):
            if i == 1:
                getids = map(lambda x: x[0], UserTester.objects.filter().values_list("user_id"))
            else:
                getids = map(lambda x: x[0], UserTester.objects.filter(**(i)).values_list("user_id"))
            if "user_id__in" not in filter_query:
                filter_query.update({"user_id__in": getids})

            if i == 1:
                age_str = "all"
                gender = "all"
            else:
                age_str = "".join(filter(lambda x: x.startswith("age"), i.keys()))
                age_str = age_str + str(i[age_str]) if age_str else "all"
                # transpose = zip(*ContentAnalysis.objects.filter(**filter_query).values_list(*select))

            filter_query.update({"media_id": media_id})

            metrics = [select[0]]+list(set(select[1:] + ['ET', "YY"]))
            select = metrics
            matrix = map(list, (ContentAnalysis.objects.filter(**filter_query).values_list(*(select))))
            df = pd.DataFrame(matrix)

            if df.empty:
                continue

            for i in df.columns:
                if i != 0:
                    df[i]=df[i].replace(to_replace=0, method='ffill')
                    df[i] = df[i].fillna(method='ffill')
            #print df[[1]].replace(to_replace=0, method='ffill')

            yy, et = select.index('YY'),select.index('ET')
            emotions = pd.DataFrame(index = df.index)#, data= df[[0,yy,et]], columns = ['seconds','yy','et'])
            emotions['seconds'] = df[[0]]
            emotions['yy'] =  df[[yy]]
            emotions['et'] =  df[[et]]
            emotions['count'] = 1

            #if all(map(lambda x : int(x)<94 ,media_id)) :
            if media_id<94 :
                emotions['emotion'] = emotions.apply(self.make_emotion1 ,axis = 1)
            else:
                emotions['emotion'] = emotions.apply(self.make_emotion, axis=1)

            for i in emotions.columns:
                if i != 0:
                    emotions[i]=emotions[i].fillna(method='ffill')#(to_replace=None,method='ffill')

            emotion_first = pd.pivot_table(emotions, values='count', index='seconds', columns='emotion',
                                           aggfunc='count', fill_value=0)

            emo_cols=emotion_first.columns
            if "alarmed" not in emo_cols:
                emotion_first["alarmed"] = 0
            if "bored" not in emo_cols:
                emotion_first["bored"] = 0
            if "happy" not in emo_cols:
                emotion_first["happy"] = 0
            if "disappointed" not in emo_cols:
                emotion_first["disappointed"] = 0

            emotion_sums = emotion_first.sum(axis=0)
            self.emotion_sums["alarmed"] += emotion_sums["alarmed"]
            self.emotion_sums["bored"] += emotion_sums["bored"]
            self.emotion_sums["happy"] += emotion_sums["happy"]
            self.emotion_sums["disappointed"] += emotion_sums["disappointed"]

            cols = range(1, df.shape[1])
            if media_id < 94:
                for i in range(1, df.shape[1]):
                    #                    df[[i]] = self.range[select[i]][0] if df[[i]] < self.range[select[i]][0] else df[[i]]
                    df[[i]] = df[[i]] - self.range[select[i]][0]
                    df[[i]] = df[[i]].div(float(self.range[select[i]][1] - self.range[select[i]][0]), axis=1).multiply(100)
                    df[[i]] = df[[i]].round(2)

            size = pd.DataFrame(df[[0]].groupby(0).size())
            if metric == "avg":
                mean = df.groupby(0).mean().fillna(0)
            else:
                mean = df.groupby(0).std().fillna(0)
            mean = mean.round(2)
            summ = mean.sum(axis=1).fillna(0)
            summ = summ.round(2)
            mean = size.join(mean)

            columns = ["second", "gender", "sum", "age", "metric", "vws"] + select[1:]

            mean[mean.shape[1]] = mean.index
            mean[mean.shape[1]] = gender
            mean[mean.shape[1]] = summ
            mean[mean.shape[1]] = age_str
            mean[mean.shape[1]] = metric
            emotion_first["seconds"] = emotion_first.index

            em_cols = emotion_first.columns.tolist()
            em_result = map(lambda x: dict(x), (map(lambda x: (zip(em_cols, x)), emotion_first.values.tolist())))
            # print "before Ordering",mean.values.tolist()
            cols = mean.columns.tolist()
            cols = cols[-5:] + cols[:-5]
            mean = mean[cols]
            mean = mean.values.tolist()
            # print "after ordering",mean
            #res = {"main_graph" : map(lambda x: dict(x), (map(lambda x: (zip(columns, x)), mean)))}
            #res.update({"emotions_graph":em_result})
            res = map(lambda x: dict(x), (map(lambda x: (zip(columns, x)), mean)))
            emotion_graph += em_result
            result = result + res
        return {"main_graph":result,"emotions_graph":emotion_graph}

    def table(self, recs, select,media=None,user=None):
        graph = {}
        for i in recs:
            if user:
                graph['user'] = user
            if media:
                graph["media"] = media
            for j in select[1:]:
                key = i["metric"] + "," + i["gender"] + "," + "".join(i["age"].replace(" ", "").split(","))
                val = {j: round(np.mean(map(lambda x: x["y"], i[j])), 2),
                       "vx": round(max(map(lambda x: x["vws"], i[j])), 2)}
                if key not in graph:
                    graph[key] = {}
                    graph[key] = val
                else:
                    graph[key].update(val)
        return graph

    def clubtogether(self, gender_result, select, emotions_graph = 0):

        params = {}
        select = select[1:]
        # nd3js Emotions Graph Commented temporarily while code is being restructured.
        # if emotions_graph:
        #     result = defaultdict(list)
        #     if club :
        #         emo_keys = filter(lambda x: x!='seconds' and x!='gender' and x!= 'age' ,club[0].keys())
        #     for i in club:
        #         gender = i["gender"]
        #         age = "-".join(i["age"].replace(" ", "").split(",")).encode()
        #         for j in emo_keys:
        #             result[j] += [{"x":i["seconds"],"y":i[j]}]
        #     return result
        ret = []
        for g in gender_result.keys():
            club = gender_result[g]
            result = {}
            for i in club:
                gender = i["gender"]
                #print i["age"]
                age = "-".join(i["age"].replace(" ", "").split(",")).encode()
                n = len(select)
                summ = i["sum"]
                metric = i["metric"]
                vws = i['vws']
                second = i["second"]
                key = gender + "," + age + "," + metric
                if key not in result:
                    result[key] = defaultdict(list)
                result[key]["sum"] += [{"vws": vws, "x": second, "y": summ}]
                for j in select:
                    result[key][j] += [{"vws": vws, "x": second, "y": i[j]}]

            for i in result:
                cols = ["gender", "age", "metric"]
                att = i.split(",")
                curr = dict(zip(cols, att))
                curr.update(result[i])
                ret.append(curr)
                #gender_result[g] = ret
        return ret

    def nd3js(self,emotions_result):
        njs_er = {}
        if emotions_result:
            keys = emotions_result[0].keys()
            for i in keys:
                if i == 'seconds':
                    continue
                njs_er[i] = map(lambda x : [x['seconds'],x[i]],emotions_result)
        return njs_er

    def chartjs(self,emotions_result):
        chartjs_er = {}
        if emotions_result:
            keys = emotions_result[0].keys()
            for i in keys:
                chartjs_er[i]=map(lambda x : x[i],emotions_result)
        return chartjs_er

    def get_final_results(self,media_name,filter_query,age, select,media_id,metrics,gender,user=None):
        emotion_result, getids, result = [], [], []
        gender_result = {}
        for i in gender:
            for metric in metrics:
                temp = filter_query.copy()
                recs = self.get_agg(i, age, temp, select, metric, media_id)
                result += recs['main_graph']
                emotion_result += recs['emotions_graph']
            gender_result[i] = result
            result = []

        result = self.clubtogether(gender_result, select)
        total = self.emotion_sums["bored"] + self.emotion_sums["alarmed"] + self.emotion_sums["disappointed"] + self.emotion_sums["happy"]
        self.emotion_sums["bored"] = round(float(self.emotion_sums["bored"])*100/total,2)  if total else 0
        self.emotion_sums["alarmed"] = round(float(self.emotion_sums["alarmed"])*100 / total, 2) if total else 0
        self.emotion_sums["disappointed"] = round(float(self.emotion_sums["disappointed"])*100 / total, 2) if total else 0
        self.emotion_sums["happy"] = round(float(self.emotion_sums["happy"])*100 / total, 2) if total else 0

        return {"media": media_name, "media_id":media_id , "graph": result, "emotions_graph": self.nd3js(emotion_result),
                "table": self.table(result, select,media_name,user),
                "emotion_sums": self.emotion_sums}

    def get_media_name(self,media_id):
        arg2 = Media.objects.filter(id=media_id).values_list("name")
        name = ""
        if arg2:
            name = map(lambda x: x[0],arg2)
            if name:
                name = name[0]
        return name

    def build_query(self, select, filter_query, metrics):
        media_ids = filter_query.pop("media_id")
        metrics = metrics if metrics else self.default_metrics
        select = select if select else self.default_select
        user = (filter_query.pop("user")) if "user" in filter_query else None

        if user=="all" or user==None :
            check_users_all= True
        else:
            check_users_all = any(map(lambda x : x == "all" ,user)) # Check users for all
        if not check_users_all:
            user = map(int,user if isinstance(user,list) else [user]) if user else None
        else:
            user= None

        age = filter_query.pop('age') if 'age' in filter_query else self.default_age
        age = self.default_age if any(map(lambda x : x["op"]=="versus",age)) else age
        gender = filter_query.pop('gender') if 'gender' in filter_query else None
        gender = ["male", "female"] if gender == "versus" or gender == None else [gender]
        filter_query = filter_query if filter_query else {}

        if user:
            gender = ["male", "female"]
            age = [{"op":"all"}]
            filter_query.update({"user_id__in":user})
            metrics = ["avg"]
        combined_result = []
        media_ids = map(int,media_ids if isinstance(media_ids,list) else [media_ids])

        table = []
        if len(media_ids)>1:
            for media_id in media_ids:
                name = self.get_media_name(media_id)
                result = self.get_final_results(name,filter_query, age, select,
                                                media_id, metrics, gender)
                table.append(result.pop("table"))
                combined_result.append(result)
        else:
            if user and media_ids:
                if "user_id__in" in filter_query:
                    filter_query.pop("user_id__in")
                media_ids = media_ids[0]
                name = self.get_media_name(media_ids)
                for uid in user:
                    filter_query["user_id"] = uid
                    uname = None
                    try :
                        uname = UserTester.objects.get(user_id=uid).name

                        result = self.get_final_results(name, filter_query, age, select,
                                                        media_ids, metrics, gender, uname)
                        result['user_id'] = uid
                        result['user'] = uname
                        table.append(result.pop("table"))
                        combined_result.append(result)
                    except:
                        continue
            elif user==None and media_ids:
                media_ids = media_ids[0]
                name = self.get_media_name(media_ids)

                result = self.get_final_results(name, filter_query, age, select,
                                                media_ids, metrics, gender)
                table.append(result.pop("table"))
                combined_result = [result]
        #emotion_graph = self.clubtogether(emotion_result, select,emotions_graph=1)

        index =  {x[0]:x[1] for x in map(lambda x : (x[1]["media_id"],x[0]),enumerate(combined_result))}
        combined_result = {"result" : combined_result,"index":index,"table":table}

        return combined_result

    def post(self, request):
        info = {}
        try:
            info = json.loads(request.body)
        except ValueError:
            return Response("Unable to process request, incorrect json", status=status.HTTP_200_OK)

        return Response(self.build_query(["second"] + info['select'] if "select" in info else None,
                                         info['filter'],
                                         info['metrics'] if 'metrics' in info else []))


class RunTest(APIView):
    def post(self, request):
        serializer = MediaTestSerializer(request.data)
        serializer.save()


class GetUserTesters(APIView):

    def get_editable(self,media_id,user_id):
        testers = Get_Testers_For_Media().fetch_editable_testers(media_id)
        tstrs = dict()
        for i in testers:
            tstrs[i['id']] = i
        testers_all = self.common_function(user_id)
        for i in testers_all:
            if i['user_id'] in tstrs:
                i['is_completed'] = tstrs[i['user_id']]['is_completed']
                i['is_selected'] = True
            else:
                i['is_completed'] = False
                i['is_selected'] = False
        return testers_all

    def common_function(self,user_id):
        user_id = get_admin_id(user_id)
        if UserTester.objects.filter(admin__user_id=user_id).count() > 0:
            tester_details = UserTester.objects.filter(admin__user_id=user_id).values("id","user_id","birth_date",
                                                                                      "user__is_active","email","gender"
                                                                                      ,"name")
        else:
            tester_details = UserTester.objects.filter(user_id=user_id).values("id","user_id","birth_date",
                                                                                      "user__is_active","email","gender"
                                                                                      ,"name")
        for i in tester_details:
            i['age']=(datetime.date.today().year - i['birth_date'].year)
            i.pop('birth_date')
            i['is_active'] = i.pop('user__is_active')
        testers = map(lambda x: {"user_id": x['user_id'], "tester_id": x['id'],'is_active':x['is_active']}, tester_details)

        validtesters = []
        for i in testers:
            user = map(lambda x: x[0], MailActivation.objects.filter(user=i['user_id']
                                                                     , email_verified=True).values_list('user_id'))
            if user:
                validtesters += user
        tester_details = filter(lambda x: x['user_id'] in validtesters, tester_details)
        return tester_details

    def post(self, request):
        user_id = request.data['user_id']
        tester_details = self.common_function(user_id)
        return Response(tester_details)


class Signup(APIView):
    # permission_classes = (IsAdminUser,)
    # authentication_classes = (QuietBasicAuthentication,)

    def send_mail(self, info):
        SendMail().send_email({"type": "ios", "name": info["name"]}, info["email"], admin=1)

    def post(self, request):
        request.POST._mutable = True
        info = request.data

        if isinstance(info, QueryDict):
            info = dict(info.iteritems())
        info["created_date"] = datetime.datetime.now()  # parser.parse(date)
        info['username'] = info['email']
        company_name = info['company_name']
        designation = info['designation']
        mobile = info['mobile']
        work_number = info['work']
        role = info["role"]
        age = 0
        admin = -1
        gender = ""
        birthdate = None
        if "birth_date" in info:
            birthdate = parser.parse(info.pop('birth_date'),dayfirst=True)
        # if "age" in info:
        #     age = info.pop('age')
        if "gender" in request.data:
            gender = info.pop('gender')
        if "admin" in info:
            admin = info.pop('admin')

        serializer = UserSerializer(data=info)
        if serializer.is_valid():
            email = User.objects.filter(email=info["email"]).values_list()
            username = User.objects.filter(username=info["username"]).values_list()

            if not email and not username:
                if role == "admin":
                    serializer.create_admin(serializer.data)
                    admin = Admin.objects.get(email=info['email'])
                    uus = AdminApiAuthSerializer(data={"admin":admin.id,"api_key":uuid.uuid4(),"name":"default"})
                    if uus.is_valid():
                        uus.save()
                    Profile(user=admin.user, company_name=company_name, designation=designation,
                            mobile=mobile, work_number=work_number).save()
                    try:
                        t = threading.Thread(target=self.send_mail, args=[info])
                        t.setDaemon(False)
                        t.start()
                        # SendMail().send_email({info["email"],info["name"])
                    except:
                        return Response({"status": "Unable to send mail"})
                elif role == "usertester":
                    try:
                        admin = Admin.objects.get(id=admin)
                    except:
                        return Response({"status": "Admin id Doesn't Exist"}, status=status.HTTP_200_OK)
                    if admin:
                        data = {"email": info["email"],
                                "username": info["username"],
                                "password": info["password"],
                                "name": info["name"],
                                "created_date": info["created_date"],
                                "role": role,
                                "birth_date": birthdate,
                                "gender": gender,
                                "admin": admin.id}
                        serializer.create_tester(data, admin)
                    # SendMail().send_mail({'type': 'ios','email':info["email"]})
                    try:
                        t = threading.Thread(target=self.send_mail, args=[info])
                        t.setDaemon(False)
                        t.start()
                        # SendMail().send_email({info["email"],info["name"])
                    except:
                        return Response({"status": "Unable to send mail"})
                        # SendMail().send_email(info["email"])
                else:
                    return Response({"status": "Role Doesn't exist"}, status=status.HTTP_200_OK)
                return Response({"status": "created"}, status=status.HTTP_200_OK)
            # return Response({"status":"Email Id {0} or Username {1} already exists".format(
            # serializer.data['email'], serializer.data['username'])}, status = status.HTTP_200_OK)
            return Response({"status": "Email Id {0} already exists".format(
                serializer.data['email'])}, status=status.HTTP_200_OK)
        return Response({"status": "error"}, status=status.HTTP_200_OK)


class GenerateApiKey(APIView):

    def checkAdmin(self,user_id):
        try:
            admin_id = Admin.objects.get(user_id=user_id).id
            return admin_id
        except:
            return False

    def generate_key(self,name,admin_id):
        api_key = uuid.uuid4()
        uus = AdminApiAuthSerializer(data={"name":name,"admin":admin_id,"api_key":api_key})
        if uus.is_valid():
            uus.save()
            return {"api_key":str(api_key)}
        else:
            return {"status":"Incorrect request data."}

    def post(self,request):
        user_id=0
        if not "admin_id" in request.data:
            user_id = request.data['user_id']
            admin_id = self.checkAdmin(user_id)
            if not admin_id:
                return Response({"status":"Admin not found."})
        else:
            admin_id = request.data['admin_id']
        name = request.data['key_name']
        response = self.generate_key(name,admin_id)
        return Response(response)


class DeleteApiKey(APIView):
    def post(self,request):
        user_id=0
        if not "admin_id" in request.data:
            user_id = request.data['user_id']
        else:
            admin_id = request.data['admin_id']
        api_key = request.data['api_key']
        try :
            if user_id:
                uus = AdminApiAuth.objects.get(admin__user_id=user_id, api_key=uuid.UUID(api_key))
            else:
                uus= AdminApiAuth.objects.get(admin = admin_id,api_key=uuid.UUID(api_key))
            uus.delete()
        except:
            return Response({"status":"Admin id and api_key don't match"})
        return Response({"status":"done"})


class ScheduleADemoView(APIView):
    def send_mail(self, info,from_email = None,reply_to=None):
        SendMail().send_email(info, info["email"], admin=3,from_address = from_email, reply_to = reply_to)

    def post(self, request):
        request.POST._mutable = True
        info = request.data
        info["requested_date"] = datetime.datetime.now()

        if isinstance(info, QueryDict):
            info = dict(info.iteritems())
        #info["message"] = " "
        #info["company_url"] = " "
        serializer = ScheduleADemoSerializer(data=info)

        if serializer.is_valid():
            serializer.save();
            try:
                    #kwargs = {'reply_to':"marketing@effectlab.io",'from_email':'update@affectlab.io'}
                    kwargs = {'reply_to': None, 'from_email':None}
                    t = threading.Thread(target=self.send_mail, args=[info],kwargs = kwargs)
                    t.setDaemon(False)
                    t.start()
            except:
                return Response({"status": "Unable to send mail"})

            return Response({"status": "created"}, status=status.HTTP_200_OK)

        return Response({"status": "error"}, status=status.HTTP_200_OK)


# class QueryContentTypes(APIView):
#     def post(self, request):
#         request.POST._mutable = True
#         info = request.data
#         query_flag = 0
#         op = request.data['op']
#         if op == "delete":
#             try:
#                 projects = map(lambda x: x[0],
#                                Project.objects.filter(content_type_id=info['content_type_id']).values_list('name'))
#                 if projects:
#                     return Response({"status": "Projetcs " + ','.join(
#                         projects) + " depend on this content type. Please delete them."})
#                 ContentType.objects.get(pk=info['content_type_id']).delete()
#             except:
#                 return Response({"status": "content type not found"})
#             return Response({"status": "deleted"})
#         if op == "update":
#             try:
#                 contenttype = ContentType.objects.get(pk=info["content_type_id"])
#             except:
#                 return Response({"status": "content type id not found"})
#             cont = ContentTypeSerializer(contenttype, data={"name": info["content_type"]}, partial=True)
#             if cont.is_valid():
#                 cont.save()
#             else:
#                 return Response({"status": "Failed"}, status=status.HTTP_200_OK)
#
#             return Response({"status": "updated"})
#
#         user_id = info['user_id']
#         user_id = get_admin_id(user_id)
#         try:
#             admin_id = Admin.objects.get(user_id=user_id)
#         except:
#             return Response({"status": "No admin with user id"})
#         if isinstance(info, QueryDict):
#             infolist = dict(info.iterlists())
#             query_flag = 1
#         if op == "add":
#             content_types = infolist['content_types'] if query_flag else info['content_types']
#             for i in content_types:
#                 found = ContentType.objects.filter(name=i, admin__user_id=user_id).count()
#
#                 if not found:
#                     serializer = ContentTypeSerializer(data={"admin": admin_id.id, "name": i})
#                     if serializer.is_valid():
#                         serializer.save()
#         if op == "get":
#             content_types = sorted(ContentType.objects.filter(admin__user_id=user_id).values_list('name', "id"),
#                                    key=lambda x: -x[1])
#             return Response(map(lambda x: x[0], content_types))
#
#         if op == "read":
#             content_types = sorted(ContentType.objects.filter(admin__user_id=user_id).values('name', "id"),
#                                    key=lambda x: -x['id'])
#             return Response(content_types)
#
#         return Response({'status': 'success'})


class SaveImages(APIView):
    def post(self, request):
        
        # serializer = VideoImageSerializer(data=request.data)
        vid = open("video_image.png", "wb")
        #vid = open(request.FILES['image'].name, "wb")
        imgstr=re.search(r'base64,(.*)', request.data['image']).group(1)
    
        vid.write(imgstr.decode('base64'))
        vid.close()
        #vid.write(request.FILES['image'].read())
        return Response()


class MindWave_Save(APIView):
    def post(self, request):
        try:
            info = json.loads(request.body())
        except ValueError:
            return Response("Unable to process request, incorrect json", status=status.HTTP_200_OK)
        for rec in info:
            serializer = MindWave(login_id=rec['loginid'],
                                  video_id=rec['videoid'],
                                  meditation=rec['meditation'],
                                  attention=rec['attention'],
                                  raw_entries="",
                                  delta=rec['delta'],
                                  theta=rec['theta'],
                                  lowAlpha=rec['lowAlpha'],
                                  highAlpha=rec['highAlpha'],
                                  lowBeta=rec['lowBeta'],
                                  highBeta=rec['highBeta'],
                                  lowGamma=rec['lowGamma'],
                                  highGamma=rec['highGamma'])
            serializer.save()
        return Response(info, status=status.HTTP_200_OK)


class QueryCategories(APIView):
    def post(self, request):
        request.POST._mutable = True
        info = {}
        info = request.data
        querydict_flag = 0
        if isinstance(info, QueryDict):
            infolist = dict(request.data.iterlists())
            querydict_flag = 1
        op = info['op']
        if op == "delete":
            try:
                projects = map(lambda x: x[0],
                               Project.objects.filter(category_id=info['category_id']).values_list('name'))
                subcategories = map(lambda x: x[0],
                                    SubCategory.objects.filter(category_id=info['category_id']).values_list('name'))
                if projects:
                    response = "Projetcs " + ','.join(projects) + " depend on this category."
                    response = response + "\t Subcategories " + ','.join(
                        subcategories) + " depend on this category" if subcategories else response
                    return Response(response)
                cat = Category.objects.get(pk=info['category_id'])
                cat.delete()
            except:
                return Response({"status": "category not found"})
            return Response({"status": "deleted"})
        user_id = int(info['user_id'])
        user_id = get_admin_id(user_id)
        if op == "get":
            filter, result = {}, []
            categories = []
            # content_types=reduce(lambda x,y:x+list(y),ContentType.objects.filter().values_list("name"),[])
            filter.update({"admin__user_id": info["user_id"]})
            if "category" in info:
                filter.update({"name": info["category"]})
            categories = list(Category.objects.filter(**filter).values("id", "name"))
            if "category" in info:
                category_id = categories[0]["id"] if categories else None
                if not category_id:
                    return Response({"status": "Category not found"},
                                    status=status.HTTP_200_OK)
                name = filter.pop("name")
                filter.update({"category": category_id})
                subcategories = {"subcategories": list(
                    SubCategory.objects.filter(**filter).values("id", "name"))}
                return Response([{"category_id": category_id,
                                  "category_name": name, "subcategories": subcategories}])

            for i in categories:
                result.append({"category_id": i["id"], "category_name": i["name"], "subcategories": sorted(list(
                    SubCategory.objects.filter(category=i["id"], admin__user_id=user_id).values("id", "name")),
                    key=lambda x: -x['id'])})
            return Response(sorted(result, key=lambda x: -x["category_id"]))


            # content_type = infolist['content_types'] if querydict_flag else info['content_types']
            # if "content_types" in info:
            #     for content_type in info['content_types']:
            #         found = map(lambda x: x[0],(ContentType.objects.filter(name=content_type).values_list('id')))
            #         if found :
            #             continue
            #         content_type = ContentTypeSerializer(data = {"name":content_type})
            #         if content_type.is_valid():
            #             content_type.save()

        if op == "update":
            category = Category.objects.get(pk=int(info["category_id"]))
            cat = CategorySerializer(category, data={"name": info["category"],
                                                     "admin__user_id": user_id}, partial=True)
            if cat.is_valid():
                cat.save()
            else:
                return Response({"status": "Failed"}, status=status.HTTP_200_OK)
            subcategories = infolist['subcategories'] if querydict_flag else info['subcategories']

            if querydict_flag:
                addsub = infolist['add_subcategories'] if 'add_subcategories' in infolist else []
            else:
                addsub = info['add_subcategories'] if 'add_subcategories' in info else []

            for subcategory in subcategories:
                try:
                    subcategoryObj = SubCategory.objects.get(id=int(subcategory["subcategory_id"]),
                                                             category_id=int(info['category_id']),
                                                             admin__user_id=user_id)
                except:
                    return Response({"status": "Category or User doesn't map to subcategories"},
                                    status=status.HTTP_200_OK)
                subcat = SubCategorySerializer(subcategoryObj,
                                               data={"name": subcategory["subcategory"],
                                                     "admin__user_id": user_id},
                                               partial=True)

                if subcat.is_valid():
                    subcat.save()
                else:
                    return Response({"status": "Failed"}, status=status.HTTP_200_OK)

            for subcategory in addsub:
                found = SubCategory.objects.filter(name=subcategory,
                                                   category=info['category_id'],
                                                   admin__user_id=user_id).count() > 0
                if found:
                    continue

                admin_id = Admin.objects.get(user_id=user_id).id
                subcat = SubCategorySerializer(data={"name": subcategory,
                                                     "category": info['category_id'],
                                                     "admin": admin_id})
                if subcat.is_valid():
                    subcat.save()
                else:
                    continue

            return Response({"status": "updated"})

        if op == "add":
            found = map(lambda x: x[0], (Category.objects.filter(name=info['category'],
                                                                 admin__user_id=user_id).values_list('id')))
            admin_id = list(Admin.objects.filter(user_id=user_id).values('id'))
            if found:
                cat_id = found[0]
                if admin_id:
                    admin_id = admin_id[0]
                else:
                    return Response({"status": "Admin not found"}, status=status.HTTP_200_OK)

                if "subcategories" in info:
                    subcategories = infolist['subcategories'] if querydict_flag else info['subcategories']
                    for subcategory in subcategories:
                        found = list(SubCategory.objects.filter(name=subcategory,
                                                                category=cat_id, admin__user_id=user_id).values_list(
                            'id'))
                        if found:
                            continue
                        subcat = SubCategorySerializer(data={"name": subcategory,
                                                             "category": cat_id,
                                                             "admin": admin_id['id']})
                        if subcat.is_valid():
                            subcat.save()
                        else:
                            continue
                        # return Response({"status": "Category exists"}, status=status.HTTP_400_BAD_REQUEST)
            else:
                category = info['category']
                if admin_id:
                    admin_id = admin_id[0]
                    cat = CategorySerializer(data={"name": category, "admin": admin_id['id']})
                    if cat.is_valid():
                        cat.save()
                        cat_id = cat.data['id']
                    else:
                        return Response({"status": cat.errors}, status=status.HTTP_200_OK)

                    if "subcategories" in info:
                        subcategories = infolist['subcategories'] if querydict_flag else info['subcategories']

                        for subcategory in subcategories:
                            found = list(SubCategory.objects.filter(name=subcategory,
                                                                    category=cat_id, admin__user_id=user_id).values_list(
                                'id'))
                            if found:
                                continue
                            subcat = SubCategorySerializer(data={"name": subcategory,
                                                                 "category": cat_id,
                                                                 "admin": admin_id['id']})
                            if subcat.is_valid():
                                subcat.save()
                            else:
                                continue

                else:
                    return Response({"status": "Admin not found"}, status=status.HTTP_200_OK)
            return Response({"status": "added"})


        return Response({"status": "invalid operation"})


class GetAssociatedIds(APIView):

    def get(self,request,media_id):
        adclutter=list(AdClutter.objects.filter(default_media_id = media_id,associated_media__is_deleted=False).values(
                'associated_media__id','associated_media__url').distinct())
        for i in adclutter:
            i['media_id'] = i.pop('associated_media__id')
            i['url'] = i.pop('associated_media__url')
        return Response(adclutter)

class Get_Edit_Dynamic_Form_Fill(APIView):
    def post(self,request):
        request.POST._mutable = True
        media_id = request.data['media_id']
        adclutter = []
        try :
            md = Media.objects.get(id = media_id)
            activitytype = md.activitytype
            activitytype = 'Video' if not activitytype else activitytype.name
            mdt=map(lambda x : x[0],MediaTest.objects.filter(media_id=media_id).values_list('id'))
            if mdt :
                mdt = mdt[0]

            hashtags = map(lambda x:x[0],MediaTestHashtag.objects.filter(mediatest_id=mdt).values_list("hashtag_id__hashtag"))
            adclutter=AdClutter.objects.filter(default_media_id = media_id,associated_media__is_deleted=False).values_list(
                        'associated_media__id','associated_media__url')

        except:
            activitytype = md.activitytype
            activitytype = 'Video' if not activitytype else activitytype.name

            return Response({'ad_clutter':list(set(adclutter)),'url':md.url,'name':md.name,'file_location':"0","hashtags":[],
                         'version':md.media_version,'activitytype':activitytype})
        print {'ad_clutter':list(set(adclutter)),'name':md.name,'file_location':str(md.file_location),'url':md.url
                            ,"hashtags":hashtags,'version':md.media_version,'activitytype':activitytype}
        return Response({'ad_clutter':list(set(adclutter)),'name':md.name,'file_location':str(md.file_location),'url':md.url
                            ,"hashtags":hashtags,'version':md.media_version,'activitytype':activitytype})


class UpdateMediaPlusMediatest(APIView):
    def post(self,request):
        request.POST._mutable = True
        info = request.data
        #info['ad_clutter'] = [] if info['media']['activitytype'] !='1' else info['ad_clutter']
        info['delete_media'] = "all" if info['media']['activitytype'] !='1' else info['delete_media']
        #info['media']['file_location'] = '0' if info['media']['activitytype'] !='1' else info['media']['file_location']
        media_response = AddMedia().update_function(info["media"],ad_clutter=info['ad_clutter'],delete_media=info['delete_media'])
        if media_response["status"] != "inserted":
            return Response(media_response)
        media_test = info["media_test"]
        media_test.update({"media_id":media_response["media_id"]})
        mediatest_response = CreateMediaTest().common_function(info["media_test"],update_media_id=media_response["media_id"]
                                                               ,ad_clutter=media_response['ad_clutter'])
        return Response(mediatest_response)


class MediaPlusMediatest(APIView):
    def post(self,request):
        request.POST._mutable = True
        info = request.data
        ad_clutter = info["ad_clutter"]
        info['media']['url'] = info['media']['url'] if 'url' in info['media'] else ""
        if not info['media']['url'] and info['media']['activitytype']==1:
            return Response({"status":"fail, Please provide url"},status=404)
        media_response = AddMedia().common_function(info["media"],ad_clutter=ad_clutter)
        if media_response["status"] != "inserted":
            return Response(media_response)
        media_test = info["media_test"]
        media_test.update({"media_id":media_response["media_id"]})
        mediatest_response = CreateMediaTest().common_function(info["media_test"],ad_clutter=media_response['ad_clutter'])
        return Response(mediatest_response)

class AddMedia(APIView):

    def update_function(self,media_data,ad_clutter=None,delete_media=None):
        path = os.path.join(settings.PROJECT_DIR + "/media/")
        image = media_data['image'] if 'image' in media_data else None
        im = image
        image = map(lambda x:x[0],list(Image.objects.filter(image_name = image).values_list('id')))

        if image:
            image = None
        else:
            image = media_data['image'] if im!=None else None

        image = {'image_name':image}
        if not os.path.isdir(path):
            os.mkdir(path)

        serializer = ImageSerializer(data=image)
        valid = serializer.is_valid()
        if valid:
            image_obj = serializer.save()
            media_data['image'] = image_obj.data['id']
        else:
            media_data['image'] = None
        if 'file_location' in media_data:
            if not media_data['file_location'].strip() :
                del media_data['file_location']
        if 'url' in media_data:
            if not media_data['url'].strip():
                del media_data['url']

        media_data['media_version'] = media_data['media_version'] if 'media_version' in media_data else None
        media_data['activitytype'] = media_data['activitytype'] if 'activitytype' in media_data else None

        if 'media_id' in media_data:
            try :
                mid = Media.objects.get(id=int(media_data['media_id']))
                media_serializer = MediaSerializer(mid,data=media_data,partial=True)
            except:
                return {"status":"Media Id Not Found"}
        else:
            media_serializer = MediaSerializer(data=media_data)
        if media_serializer.is_valid():
            media_serializer.save()
            name = media_serializer.data['name']
            ad_media = {'project':media_serializer.data['project'] , 'media_version': media_serializer.data['media_version'],
                        'file_location': media_serializer.data['file_location'],
                        'image': media_serializer.data['image'], 'activitytype': media_serializer.data['activitytype'],
                        'is_default': False,
                        'created_date': media_serializer.data['created_date'],
                        'is_deleted': False}

            cnt = 0
            clutter_ids = []
            if isinstance(delete_media,str) and delete_media == 'all':
                delete_media=map(lambda x : x[0],AdClutter.objects.filter(default_media_id = media_serializer.data['id']).values_list('associated_media_id'))

            if isinstance(delete_media,list):
                for i in delete_media:
                    try:
                        delete = Media.objects.get(id=i)
                        delete.is_deleted = True
                        delete.save()
                    except:
                        continue
                        pass

            for i in ad_clutter:
                if int(i['id'])<=0:
                    ad_media.update({'name':name+'_'+str(cnt),'url':i['value']})
                    mser = MediaSerializer(data=ad_media)
                    cnt+=1
                else:
                    try:
                        med = Media.objects.get(id=i['id'])
                        mser = MediaSerializer(med,data={'url':i['value']},partial=True)
                    except:
                        continue
                if mser.is_valid():
                    mser.save()
                    clutter_ids.append(mser.data['id'])
                    adclutter = AdClutter(default_media_id=media_serializer.data["id"],
                                          associated_media_id=mser.data['id'])
                    adclutter.save()

            return {"status": "inserted","media_id":media_serializer.data["id"],'ad_clutter':clutter_ids}
        return {"status": media_serializer.errors}

    def common_function(self,media_data,ad_clutter=None):
        path = os.path.join(settings.PROJECT_DIR + "/media/")
        image = media_data['image'] if 'image' in media_data else None
        im = image
        image = map(lambda x:x[0],list(Image.objects.filter(image_name = image).values_list('id')))
        project_id = media_data['project']
        if 'name' in media_data and 'project' in media_data:
            media = Media.objects.filter(name=media_data['name'].strip(),is_deleted = False,
                                         project_id=project_id).values_list("name",flat=True)
            if media:
                return {'status':'media with same name already exists'}

        if image:
            image = None
        else:
            image = media_data['image'] if im!=None else None

        image = {'image_name':image}
        if not os.path.isdir(path):
            os.mkdir(path)

        serializer = ImageSerializer(data=image)
        valid = serializer.is_valid()
        if valid:
            image_obj = serializer.save()
            media_data['image'] = image_obj.data['id']
        else:
            media_data['image'] = None
        media_data['created_date'] = datetime.datetime.today()
        media_data['file_location'] = media_data['file_location'] if 'file_location' in media_data else None
        media_data['url'] = media_data['url'] if 'url' in media_data else None
        media_data['media_version'] = media_data['media_version'] if 'media_version' in media_data else None
        media_data['activitytype'] = media_data['activitytype'] if 'activitytype' in media_data else None
        media_data['is_default'] = True

        if 'id' in media_data:
            try :
                mid = Media.objects.get(id=media_data['id'])
                if 'name' in media_data and 'project' in media_data:
                    media = Media.objects.filter(name=media_data['name'].strip(), is_deleted=False,
                                                 project_id=media_data['project']).values_list("name", flat=True)
                    if media:
                        return {'status': 'media with same name already exists'}

                media_serializer = MediaSerializer(mid,data=media_data,partial=True)
            except:
                return {"status":"Media Id Not Found"}
        else:
            media_serializer = MediaSerializer(data=media_data)
        clutter_ids = []
        if media_serializer.is_valid():
            media_serializer.save()
            try:
                if media_data['file_location'] == str(1):
                    m = Media.objects.get(id=media_serializer.data['id'])
                    m.url =  m.url.replace("/C:\\fakepath\\/", '')
                    m.save()
            except:
                pass
            default_media_id = media_serializer.data['id']
            name = media_data['name']
            for i in xrange(len(ad_clutter)):
                media_data['url']  = ad_clutter[i]
                media_data['name'] = name+'_'+str(i)
                media_data['is_default'] = False
                media_serializer = MediaSerializer(data=media_data)

                if media_serializer.is_valid():
                    media_serializer.save()
                try:
                    if media_data['file_location'] == str(1):
                        m = Media.objects.get(id = media_serializer.data['id'])
                        m.url = m.url.replace("/C:\\fakepath\\/", '')
                        m.save()
                except:
                    pass
                adclutter = AdClutter(default_media_id=default_media_id,associated_media_id=media_serializer.data['id'])
                adclutter.save()
                clutter_ids.append(adclutter.id)
            return {"status":"inserted","media_id":default_media_id,'ad_clutter':clutter_ids}
        else:
            return {"status": media_serializer.errors}


    def post(self, request, *args, **kwargs):
        media_data = request.data.copy()
        response = self.common_function(media_data)
        return Response(response)
        #return Response({"status": serializer.errors})


class ISDeleted(APIView):
    def post(self,request):
        request.POST._mutable = True
        try:
            if "media_id" in request.data:
                obj = Media.objects.get(id=request.data["media_id"])
            elif "project_id":
                obj = Project.objects.get(id=request.data["project_id"])
            obj.is_deleted = True
            obj.save()
            return Response({"status":"deleted"})
        except:
            response = "media_id not found" if "media_id" in request.data else "project_id not found"
            return Response({"status":response})


class UserProjectDetails(APIView):
    def jsonapiformat(self,projects):
        result = {}
        result['data'] = []
        for p in projects:
            p['type'] = 'project'
            p['attributes'] = {'name':p.pop('name'),'created_date':p.pop('created_date')}
            p["id"] = p.pop('project_id')
            p['relationships'] = {'list_of_tests' :{'data':p.pop('list_of_tests')}}
            for t in p['relationships']['list_of_tests']['data']:
                t['attributes'] = {'name':t.pop('name')}
                t['id'] = t.pop('test_id')
                t['type'] = 'test'
                t.pop('testers')
                # t['testers'] = {'data':t.pop('testers')}
                # for user in t['testers']['data']:
                #     user['type'] = 'tester'
                #     user['atribute'] = {'name':user.pop('name')}

            result['data'].append(p)
        return result

    def get(self, request,api_key):
        request.POST._mutable = True
        token = api_key
        admin_id = ApiWrapper().token_valid_check(token)
        if not admin_id:
            return Response({"status": "Api key not found"})
        projects = list(Project.objects.filter(admin_id=admin_id,is_deleted=False).values("id","name","created_date"))
        for project in projects:
            media = list(Media.objects.filter(project_id=project['id'],is_deleted=False).values("id","name"))
            for med in media:
                med['test_id'] = med.pop('id')
                med['testers'] = Get_Testers_For_Media().fetch_testers(med['test_id'])
            project['list_of_tests'] = sorted(media, key=lambda x: -x['test_id'])
            project['project_id'] = project.pop('id')
        projects = self.jsonapiformat(projects)
        return Response(projects)


class getProjectList(APIView):

    def jsonapiformat(self,project):
        result = {}
        result['data'] = {'type':'project'}
        result['data']['attributes'] = {'name':project.pop('name'),'created_date':project.pop('created_date')}
        result['data']["id"] = project.pop('id')
        result['data']['relationships'] = {'list_of_tests' :{'data':project.pop('list_of_tests')}}
        for t in result['data']['relationships']['list_of_tests']['data']:
            t['attributes'] = {'name':t.pop('name')}
            t['id'] = t.pop('test_id')
            t['type'] = 'test'
        return result


    def get(self,request,api_key=None,project_id=None):
        if not api_key:
            api_key = request.GET.get('api_key')
            project_id = request.GET.get('project_id')
            if not api_key:
                return Response({"status":"Please provide api_key"})
        token = api_key
        admin_id = ApiWrapper().token_valid_check(token)
        if not admin_id:
            return Response({"status": "Api key not found"})

        if not project_id:
            projects = getProjectList1().getProjects(admin_id)
            return Response(projects)
        try:
            project = Project.objects.get(admin_id=admin_id,is_deleted=False,id=project_id)
        except:
            return {"status":"Project not found for api_key or not Authorised"}

        result = {}

        media = list(Media.objects.filter(project_id=project_id,is_deleted=False).values("id","name"))
        for med in media:
            med['test_id'] = med.pop('id')
        result['created_date'] = project.created_date
        result['name'] = project.name
        result['list_of_tests'] = sorted(media, key=lambda x: -x['test_id'])
        result['id'] = project.id
        result = self.jsonapiformat(result)
        return Response(result)


class getEditableTesters(APIView):

    def post(self,request):
        if 'check_complete' in request.data:
            return Response({'count':Get_Testers_For_Media().get_complete(request.data['media_id'])})
        testers = GetUserTesters().get_editable(request.data['media_id'], request.data['user_id'])
        return Response(testers)



class getProjectList1(APIView):
    def jsonapiformat(self,projects):
        result = {}
        result['data'] = []
        for p in projects:
            p['type'] = 'project'
            p['attributes'] = {'name':p.pop('name'),'created_date':p.pop('created_date')}
            result['data'].append(p)
        return result

    def getProjects(self,admin_id):
        projects = list(Project.objects.filter(admin_id=admin_id,is_deleted=False).values("id","name","created_date"))
        projects = self.jsonapiformat(projects)
        return projects


    def get(self, request,api_key):
        if not api_key:
            api_key = request.GET.get('api_key')
            if not api_key:
                return Response({"status":"Please provide api_key"})
        token = api_key
        admin_id = ApiWrapper().token_valid_check(token)
        if not admin_id:
            return Response({"status": "Api key not found"})
        projects=self.getProjects(admin_id)
        return Response(projects)


class SelectUserProject(APIView):
    def post(self, request):
        request.POST._mutable = True
        userid = request.data['user_id']
        userid = get_admin_id(userid)
        if Admin.objects.filter(user_id=userid).count() > 0:
            projects = list(Project.objects.filter(admin__user_id=userid,is_deleted=False).values())
        else:
            adminid = map(lambda x: x[0], UserTester.objects.filter(user_id=userid).values_list("admin_id"))
            if adminid:
                adminid = adminid[0]
                projects = list(Project.objects.filter(admin_id=adminid).values())
            else:
                return Response([])
        for project in projects:
            media = list(Media.objects.filter(project_id=project['id'],is_deleted=False,is_default=True).values())
            for med in media:
                img = list(Image.objects.filter(id=med['image_id']).values())
                med['image_name'] = img[0]['image_name'] if img else ""
                med['media_id'] = med.pop('id')
                med['testers'] = Get_Testers_For_Media().fetch_testers(med['media_id'])
            project['media'] = sorted(media, key=lambda x: -x['media_id'])
            project['project_id'] = project.pop('id')
        return Response(projects)


class CreateEntity(APIView):
    def create_image(self, image):
        image = ImageSerializer(data={"url": image})
        if image.is_valid():
            image.save()
        return image

    def post(self, request, option="add", entity="project"):
        request.POST._mutable = True

        if option == "add" or option == "update":
            if entity != "media":
                user_id = request.data['user']
                user_id = get_admin_id(user_id)
            request.data['created_date'] = datetime.datetime.now()  # parser.parse(request.data['created_date'])
            if entity == "project":

                if option == "update":
                    pk = request.data['project_id']

                    request.data['subcategory'] = SubCategory.objects.filter(
                        name=request.data['subcategory'],
                        admin__user_id=user_id
                    ).values_list("id")
                    request.data['subcategory'] = request.data['subcategory'][0][0] if request.data[
                        'subcategory']  else None

                    request.data['category'] = Category.objects.filter(
                        name=request.data['category'],
                        admin__user_id=user_id
                    ).values_list("id")
                    request.data['category'] = request.data['category'][0][0] if request.data['category']  else None

                    fkfields = ['category', 'subcategory']#, 'content_type']
                    if not all(map(lambda i: request.data[i], fkfields)):
                        return Response({"status": "Failed"}, status=status.HTTP_200_OK)
                    try:
                        project = Project.objects.get(pk=pk)
                    except:
                        return Response({"status": "Failed"}, status=status.HTTP_200_OK)
                    serializer = ProjectSerializer(project, data=request.data, partial=True)
                    if serializer.is_valid():
                        serializer.save()
                        return Response({"status": "Updated"}, status=status.HTTP_200_OK)
                    else:
                        return Response(serializer.errors, status=status.HTTP_200_OK)
                else:
                    # image = self.create_image(request.data['image'])
                    # request.data['image'] = image.data['id']
                    same = Project.objects.filter(name = request.data['name'],is_deleted=False).values_list("name",flat=True)
                    if same:
                        return Response({'status':"project with same name already exists"})

                    created_date = request.data['created_date']
                    cat_name = request.data['category']
                    subcat_name = request.data['subcategory']
                    category = Category.objects.filter(name=cat_name,
                                                       admin__user_id=user_id).values_list('id')
                    subcategory = None
                    image = request.data['image']
                    if category:
                        subcategory = SubCategory.objects.filter(name=subcat_name,
                                                                 admin__user_id=user_id,
                                                                 category_id=category[0]).values_list('id')

                        subcategory = subcategory[0][0] if subcategory else None
                        category = category[0][0]
                    # content_type = request.data['content_type']
                    # content_type = ContentType.objects.filter(name=content_type,
                    #                                           admin__user_id=user_id).values_list('id')
                    #
                    # content_type = content_type[0][0] if content_type else None
                    if not (subcategory and category):# and content_type):
                        return Response({"status": "Failed - Invalid Category or Subcategory"},#, or Content Type"},
                                        status=status.HTTP_200_OK)
                    user = None
                    try:
                        user = Admin.objects.get(user_id=request.data['user'])
                    except:
                        pass
                    user = user.id if user else None

                    # request.data['subcategory']=subcategory
                    # request.data['category']=category
                    # request.data['content_type']=content_type.data['id']
                    request.data['user'] = user
                    info = request.data.copy()
                    admin = info['user']
                    data = {'category': category, "subcategory": subcategory,
                            "admin": admin, "image": image, "created_date": created_date,
                            #"content_type": content_type,
                            "name": info["name"],"is_deleted":False}
                    serializer = ProjectSerializer(data=data)

            elif entity == "media":
                pk = request.data['media_id']
                if option == "update":
                    try:
                        request.data['image'] = self.create_image(request.data['image']).data['id']
                        image_id = request.data['image'].data['id']
                        path = os.path.join(settings.BASE_DIR + "/images/")
                        if not os.path.isdir(path):
                            os.mkdir(path)
                        path = os.path.join(settings.BASE_DIR + "/images/" + str(pk))
                        if not os.path.isdir(path):
                            os.mkdir(path)

                        filename = os.path.join(settings.BASE_DIR + "/images/", str(pk),
                                                image_id + "_" + str(request.FILES['file'].name))

                        request.data.pop("media_id")
                        request.data['project_id'] = int(request.data.pop('project')[0])
                        img = open(filename, "wb")
                        img.write(request.FILES["file"].read())
                    except:
                        return Response({"status": "Failed Upload"}, status=status.HTTP_200_OK)
                    try:
                        media = Media.objects.get(pk=pk)
                    except:
                        return Response({"status": "Media Id Not Found"}, status=status.HTTP_200_OK)
                    serializer = MediaSerializer(media, data=request.data, partial=True)
                    if serializer.is_valid():
                        serializer.save()
                        return Response({"status": "Updated"}, status=status.HTTP_200_OK)
                    else:
                        return Response(serializer.errors, status=status.HTTP_200_OK)
                else:
                    # image=self.create_image(request.data['image'])
                    # request.data['image'] = image.data['id']
                    # data = dict(request.data.iterlists())
                    request.data['image'] = self.create_image(request.data['image']).data['id']
                    image_id = request.data['image']
                    serializer = MediaSerializer(data=request.data)
                    if serializer.is_valid():
                        serializer.save()
                    else:
                        return Response({"status": "Failed"}, status=status.HTTP_200_OK)
                    try:
                        pk = serializer.data['id']
                        path = os.path.join(settings.BASE_DIR + "/images/")

                        if not os.path.isdir(path):
                            os.mkdir(path)
                        path = os.path.join(settings.BASE_DIR + "/images/" + str(pk))
                        if not os.path.isdir(path):
                            os.mkdir(path)

                        filename = os.path.join(settings.BASE_DIR + "/images/",
                                                str(pk), str(image_id) + "_" + str(request.FILES['file'].name))
                        img = open(filename, "wb")
                        img.write(request.FILES["file"].read())
                        request.data.pop("file")
                        return Response({"status": "Entity Created"}, status=status.HTTP_200_OK)
                    except:
                        try:
                            Media.objects.get(pk=serializer.data["id"]).delete()
                        except:
                            pass
                        return Response({"status": "Failed Upload"}, status=status.HTTP_200_OK)

            else:
                serializer = UserSerializer(data=request.data)
                user = serializer.create_user(serializer.data)
            if serializer.is_valid():
                serializer.save()
                return Response({"status": "Entity Created","id":serializer.data["id"]}, status=status.HTTP_200_OK)

        elif option == "delete":
            try:
                pk = request.data['id']
                if entity == "project":
                    try:
                        project = Project.objects.get(pk=pk)
                        project.is_deleted = True
                    except:
                        return Response({"status": "Failed"}, status=status.HTTP_200_OK)

                    serializer = ProjectSerializer(project, data=request.data, partial=True)
                    if serializer.is_valid():
                        serializer.save()

                elif entity == "media":
                    try:
                        media_test = MediaTest.objects.get(pk=pk)
                        media_test.is_deleted = True
                    except:
                        return Response({"status": "Failed"}, status=status.HTTP_200_OK)

                    serializer = MediaTestSerializer(media_test, data=request.data, partial=True)
                    if serializer.is_valid():
                        serializer.save()

                return Response({"status": "deleted"}, status=status.HTTP_200_OK)
            except:
                traceback.print_exc(file=sys.stdout)
                return Response({"status": "Failed"}, status=status.HTTP_200_OK)
        return Response({"status": "Failed"}, status=status.HTTP_200_OK)


class SelectProject(APIView):
    def post(self, request):
        request.POST._mutable = True
        project = Project.objects.filter(id=request.data['project_id']).values()
        return Response(project[0] if project else {}, status=status.HTTP_200_OK)


def aggregate(arr):
    attention, meditation, BP, ET, YF, Blink, YY, AL, AP, ME, CR, CP = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0

    if arr:
        attention = np.mean(map(lambda x: x['attention'], arr))
        meditation = np.mean(map(lambda x: x['meditation'], arr))
        BP = np.mean(map(lambda x: x['BP'], arr))
        ET = np.mean(map(lambda x: x['ET'], arr))
        YF = np.mean(map(lambda x: x['YF'], arr))
        Blink = np.mean(map(lambda x: x['Blink'], arr))
        YY = np.mean(map(lambda x: x['YY'], arr))
        AL = np.mean(map(lambda x: x['AL'], arr))
        AP = np.mean(map(lambda x: x['AP'], arr))
        ME = np.mean(map(lambda x: x['ME'], arr))
        CR = np.mean(map(lambda x: x['CR'], arr))
        CP = np.mean(map(lambda x: x['CP'], arr))

    result = {"attention": attention, "meditation": meditation, "BP": BP, "ET": ET, "YF": YF,
              "Blink": Blink, "YY": YY, "AL": AL, "AP": AP, "ME": ME, "CR": CR, "CP": CP}
    return result


def custom_aggregate(arr, select):
    result = {}
    if arr:

        for i in select:
            result.update({i: round(np.mean(map(lambda x: x[i] if x[i] else 0, arr)), 2)})
    else:
        for i in select:
            result.update({i: 0})
    return result


class ResetPasswordEmail(APIView):
    def post(self, request):
        try:
            user = User.objects.get(email=request.data["email"])
        except:
            return Response({"status": "User doesn't exist"})
        try:
            mail_actv = MailActivation.objects.latest(email=request.data["email"])
        except:
            return Response({"status": "Account never activated"})

        msg = "Hi , please follow the link for resetting your account password %s" % (
            settings.API_HOSTNAME + "reset_password/" + mail_actv)

        try:
            SendMail.send_mail({"type": "ios", "email": request.data["email"]}, msg)
        except:
            return Response({"status": "Unable to send mail"})


class ForgotPassword(APIView):
    def post(self, request):
        var = request.data['variable']
        try:
            mail = MailActivation.objects.get(activation_key=var)
        except:
            return Response({"status": "Account not activated before"})
        mail.user.set_password(request.data['password'])
        mail.user.save()
        return Response({"status": "success"})


class ProjectOverview(APIView):
    def project_overview(self, user_id, projects, select=None):
        contentanalysis, result = [], []
        for pid in projects:
            # media_id = map(lambda x: x[0], (MediaTest.objects.filter(project_id=pid['id']).values_list('media_id')))
            # contentanalysis = []
            # for mid in media_id:
            #     contentanalysis_curr = list(ContentAnalysis.objects.filter(
            #         media_id=mid
            #     ).values(*select))

            #    contentanalysis += contentanalysis_curr
            result += [{"project_name": pid['name'],
                        "project_id": pid["id"],
                        "subcategory":pid['subcategory__name'],
                        "category": pid['category__name']}]
                        #"content_type":pid['content_type__name']
             #           "content_analysis": custom_aggregate(contentanalysis, select)}]
        return sorted(result,key=lambda x : -int(x['project_id']))

    def post(self, request):
        request.POST._mutable = True
        user_id = request.data["user_id"]
        user_id = get_admin_id(user_id)
        projects = list(Project.objects.filter(admin__user_id=user_id,is_deleted=False).values('id', 'name','category__name',
                                                            #'content_type__name',
                                                            'subcategory__name').distinct())
        data = request.data
        queryflag = 0
        if isinstance(data, QueryDict):
            queryflag = 1
            datalist = dict(data.iterlists())
        if 'select' in data:
            select = datalist['select'] if queryflag else data['select']
        else:
            select = ['attention', 'YY', 'ME', 'ET', 'AP']
        result = self.project_overview(user_id, projects, select)
        return Response(result, status=status.HTTP_200_OK)


class EditTest(APIView):
    def post(self,request):
        mediatest_id = request.data['mediatest_id']
        activitytype = request.data['activitytype']
        try:
            mediatest = MediaTest.objects.get(id=mediatest_id)
            media = mediatest.media
        except:
            return Response({"status":"media_test_id not found"})
        try:
            activitytype = ActivityType.objects.get(activitytype=activitytype)
            media.activitytype = activitytype
        except:
            pass
        if 'media_version' in request.data:
            media_version = request.data['media_version']
            Media.media_version = media_version
        if 'end_date' in request.data:
            end_date = request.data['end_date']
            end_date = parser.parse(end_date, dayfirst=True)
            mediatest.end_date = end_date
            if end_date <= datetime.datetime.now():
                mediatest.is_completed = True
            else:
                mediatest.is_completed = False
        if "name" in request.data:
            Media.name = request.data['name']
        if "hashtag" in request.data:
            Media.hashtag = request.data['hashtag']


        mediatest.save()
        media.save()
        return Response({"status":"success"})


class MediaOverview(APIView):
    def media_overview(self, project_id, select=[]):
        result = []
        media = list(Media.objects.filter(project=project_id,is_deleted=False,is_default=True).values('id', 'name', 'image__image_name').distinct())
        contentanalysis = []
        for med in media:
            media_id = med['id']
            activitytype = map(lambda x : x[0],list(Media.objects.filter(id = media_id).values_list('activitytype__value')))
            activitytype = activitytype[0] if activitytype else None
            contentanalysis_curr = list(ContentAnalysis.objects.filter(media_id=media_id).values(*select))
            mediacompleted = list(MediaTesterMapping.objects.filter(media_test__media_id=media_id).values_list(
                'user_tester_id', 'is_completed').distinct())
            total = len(mediacompleted)
            completed = len(filter(lambda x: x[1] == True, mediacompleted))
            result += [{"media_name": med['name'],
                        "image_name": med['image__image_name'],
                        "activity_type":activitytype,
                        "media_id": media_id,
                        "project_id": project_id,
                        "status": round((completed / float(total)) * 100 if total else 0, 2),
                        "content_analysis": custom_aggregate(contentanalysis_curr, select)}]
        return sorted(result,key = lambda x : -int(x['media_id']))



    def post(self, request):
        request.POST._mutable = True
        # user_id = request.data['user_id']
        project_id = request.data['project_id']
        data = request.data
        queryflag = 0
        if isinstance(data, QueryDict):
            queryflag = 1
            datalist = dict(data.iterlists())
        if 'select' in data:
            select = datalist['select'] if queryflag else data['select']
        else:
            select = ['attention', 'YY', 'ME', 'ET', 'AP']
        result = self.media_overview(project_id, select)
        return Response(result, status=status.HTTP_200_OK)


class SignUpView(bracesviews.AnonymousRequiredMixin,
                 bracesviews.FormValidMessageMixin,
                 generic.CreateView):
    form_class = forms.SignupForm
    model = User
    template_name = 'dashboard/activate_email.html'
    success_url = reverse_lazy('accounts:login')
    form_valid_message = "You're signed up"  # , Please Check your activation link in E-Mail and then login!"

    def form_valid(self, form, email):
        """
        Validation and create a new user
        """
        r = super(SignUpView, self).form_valid(form)
        password = form.cleaned_data["password1"]
        company_name = form.cleaned_data["company_name"]
        gender = form.cleaned_data["gender"]
        username = form.cleaned_data["username"]
        user = get_object_or_404(User,email=email)
        profile = get_object_or_404(MailActivation, user=user.id)
        profile.email_verified = True
        profile.save()
        return r


class Get_Testers_For_Media(APIView):

    def get_complete(self,media_id):
        return MediaTesterMapping.objects.filter(media_test__media_id=media_id,is_completed=True).count()

    def fetch_editable_testers(self,media_id):
        testers = sorted(MediaTesterMapping.objects.filter(media_test__media_id=media_id).values(
                        "user_tester__user_id","user_tester__name","is_completed").distinct(),
                         key = lambda x : x["user_tester__name"])
        return filter(lambda x : x['id']!=None,(map(lambda x: {"id": x["user_tester__user_id"], "name": x["user_tester__name"],
                               "is_completed":x["is_completed"]}, testers)))


    def fetch_testers(self,media_id):
        testers = sorted(MediaTesterMapping.objects.filter(media_test__media_id=media_id,is_completed=True).values(
                        "user_tester__user_id","user_tester__name").distinct(), key = lambda x : x["user_tester__name"])
        return filter(lambda x : x['id']!=None,(map(lambda x: {"id": x["user_tester__user_id"], "name": x["user_tester__name"]}, testers)))

    def get(self, request,media_id):
        return Response(self.fetch_testers(media_id))



class TestReminderMail(APIView):

    def post(self,request):
        days = request.data["days"]
        list_testers = map(list,list(MediaTesterMapping.objects.filter(is_completed=False,
        media_test__end_date__gt=datetime.datetime.now() - timedelta(days= days),
        media_test__end_date__lt = datetime.datetime.now()).values_list(
            "media_test__admin__name","user_tester__name","user_tester__user__email"
        ,"media_test__end_date","user_tester_id","media_test__media__name")))

        try :
            for tester in list_testers:
                tester = {"type": "ios", "name":tester[1],"end_date":str(tester[3]).split(".")[0],"media":tester[5],"admin":tester[0]}
                SendMail().send_email(tester,tester[2],admin=2)
        except Exception as e :
            pass
        return Response({"status":"sending mail"})


class TesterMediaList(APIView):
    def post(self, request):
        info = request.data
        user_id = info['user_id']
        mediatests = map(lambda x: x[0], (MediaTesterMapping.objects.filter(
            user_tester__user_id=user_id, is_completed=False,media_test__media__is_deleted=False,
            media_test__project__is_deleted=False,media_test__is_completed = False).values_list('media_test_id'
                                                                                         ).distinct('media_test_id')))
        test_table = []
        cnt  =0
        for mt_id in mediatests:
            test = list(MediaTest.objects.filter(id=mt_id).values('end_date', 'media_id', 'project_id', 'id').distinct())
            cnt+=1

            if test:
                test = test[0]
                end_date = test['end_date']
                tzinfo = test['end_date'].tzinfo
                if datetime.datetime.now(tzinfo) > end_date:
                    
                    mt = MediaTest.objects.get(id=test['id'])
                    mt.is_completed = True
                    mt.save()
                    continue
                project = map(lambda x: x[0], Project.objects.filter(id=test['project_id']).values_list('name'))
                media = list(Media.objects.filter(id=test['media_id'],is_deleted=False,is_default=True).values('name', "image_id","file_location"
                                                                              ,"media_version","url","activitytype_id").distinct())
                #print settings.API_ENDPOINT + "media/" + image
                if media and project:
                    
                    media = media[0]
                    image = map(lambda x: x[0], Image.objects.filter(id=media['image_id']).values_list('image_name'))
                    image = "image_not_found.png" if not image else image[0]
                    
                    test_table.append({'completion_date': str(end_date.date()), 'media_id': test['media_id'],
                                           'project_id': test['project_id'], "project_name": project[0],
                                           "media_name": media['name'],"file_location":media['file_location'],
                                           "url":media['url'],'media_version':media['media_version'],
                                            "activitytype_id":media['activitytype_id'],
                                           "image_url": settings.API_ENDPOINT + "media/" + image})

        return Response(test_table)

from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context


class AddClient(APIView):
    def post(self,request):
        name = request.data['client_name']
        dcs = DeviceClientSerializer(data={'name':name})
        if dcs.is_valid():
            dcs.save()
            return Response({"status":"done"})
        return Response({"status":"error while saving"})


class AddVersionSupport(APIView):
    def post(self,request):
        request.POST._mutable = True
        version = request.data['version']
        is_supported = request.data['is_supported']
        device_type = request.data['device_type']
        client_id = request.data['client_id']
        url = request.data['url'] if "url" in request.data else ""
        data = {"version":version,"client":client_id,"device_type":device_type,"is_supported":is_supported,'url':url[:2500]}
        vss = VersionSupportSerializer(data=data)
        if vss.is_valid():
            vss.save()
            return Response({"status":"done"})
        return Response({"status":"error while saving"})


class CheckSupportedVersion(APIView):
    def post(self,request):
        request.POST._mutable = True
        version,device_type,client = int(request.data["version"]),request.data["device_type"],int(request.data["client_id"])
        vs = VersionSupport.objects.filter(device_type=
                        device_type,client=client).values("is_supported", "version","id","url")

        max_supported_version = filter(lambda x : x['is_supported']==True,vs)
        max_url = max(max_supported_version, key=lambda x: x["version"]) if max_supported_version else ""
        is_supported = filter(lambda x : x["version"] <= version and x['is_supported']==True,vs)

        url = max(is_supported,key = lambda x : x["version"]) if is_supported else ""
        url = url["url"] if url and 'url' in url else ""
        is_supported = map(lambda x : x["is_supported"],is_supported)

        if any(is_supported):
            # start = (vs[0]["id"],vs[0]["version"])
            # max_id,max_version=reduce(lambda x,y : (x[0],y["version"])
            #                    if y['is_supported'] and y["id"] > x[0] else x,vs,start)
            # return Response({"status": "Not supported","Latest Version":max_version},
            #                 status=status.HTTP_200_OK)
            return Response({"status": True,"url":url})
        else:
            min_supported_version = max_url["version"] if max_url and 'version' in max_url else ""
            max_url = max_url["url"] if max_url and 'url' in max_url else ""
            if max_url:
                return Response({"status": False,"url":max_url,"min_supported_version":min_supported_version})
            return Response({"status": False, "url": "","min_supported_version":"Version not found for client or device"})


class SendMail:
    def __init__(self):
        self.msg = """Hey %s,
                            You are invited by %s ,
                            Your activation for Affectlab is pending, here is your activation link %stester/signup/%s"""
        # self.admin_msg = "Hey %s, Thank you for signing up with Affectlab, please follow the link for verification " \
        #                 "%sdashboard/activate_admin/%s"
        self.admin_msg = """Hey %s,
                                Thank you for expressing interest in AffectLab. Our representative will get in touch
                                shortly to understand your specific requirements and onboard your team onto our
                                platform."""
        self.admin_template = get_template("dashboard/signup_email.html")
        self.tester_msg = """Hey %s,
                                    This is a reminder for the test assigned to you by %s for the media %s.
                                    The test would expire on %s, please take the test before it ends.
                            Regards,
                                    Affectlab Team
                                """

        self.info_msg = """

                            User Details,
                                                Name  -  %s
                                                Email  -  %s
                                                Company Name  -  %s
                                                Company URL  -  %s
                                                Mobile  -  %s
                                                Message  -  %s
        """
        self.info_msg1= """

                            User Details,
                                                Name   -   %s
                                                Company Name -  %s
                                                Designation - %s
                                                Email - %s
                                                Mobile  -   %s
                                                Work Number  -   %s
                                                Created Date - %s
        """

        self.facial_msg = """
                            User Details,
                                         Name      -   {name}
                                         Mobile    -   {mobile}
                                         Email     -   {email}
                                         Video Url -   {video_url}

        """



    def render_admin(self,name):
        context = Context({'username':name})
        return self.admin_template.render(context).encode('ascii','ignore')

    def render_user_full_name(self,name):
        context = Context({'name':name})
        return self.admin_template.render(context).encode('ascii','ignore')

    def render_custom(self,context,template_url):
        template=get_template(template_url)
        context = Context(context)
        return template.render(context).encode('ascii','ignore')


    def send_email(self, request, user_email, admin=0,from_address=None, reply_to = None, context = None, template_url = None):

        if context and template_url:
            self.send_mail({"email": user_email},self.render_custom(context,template_url),html=1,
                           subject="Affect Lab Facial: Emotional Analytics Report",from_address=from_address,
                           reply_to=reply_to,info_mail=0)
            self.send_mail({"email": "info@entropiktech.com"},self.facial_msg.format(**request),
                           subject="Affect Lab Facial Coding: User Details",info_mail=0)

        elif admin ==2:
            self.send_mail({"email": user_email},self.tester_msg%
                           (request["name"],request["admin"],request["media"],request["end_date"])
                           )
        elif admin ==3:
            self.send_mail({ "email": user_email},
                           self.render_user_full_name(request["name"]),html=1,subject="Hello From Affect Lab!",from_address =from_address,reply_to=reply_to)

            #self.send_mail({"email": "marketing@affectlab.io"},
            self.send_mail({"email": "bharat.shekhawat@affectlab.io"},
                           self.info_msg % (request["name"],
                                            request["email"]
                                            ,request["company_name"],
                                            request["company_url"], request["mobile"],
                                            request["message"]),subject="Affectlab User Demo Request")
        else:
            user = list(User.objects.filter(email=user_email).values())
            if user:
                user = user[0]
                activation_key = generate_activation_key(user_email)
                data = {"user": int(user['id']),
                        "email_verified": False, "activation_key": str(activation_key),
                        "link_date": datetime.datetime.now(), "is_expired": False}
                ids = map(lambda x: x[0], MailActivation.objects.filter(user=int(user['id'])).values_list('id'))

                for i in ids:
                    ma = MailActivation.objects.get(pk=i)
                    ma.is_expired = True
                    ma.save()

                user_profile = MailActivationSerializer(data=data)
                is_valid = user_profile.is_valid()

                admin_vals = Admin.objects.get(user_id = get_admin_id(user["id"]))
                username = user["first_name"] if user["first_name"] else user["username"]
                if is_valid:
                    user_profile.save()
                    if not admin:
                        self.send_mail({"email" : user_email}, self.msg %
                                        (username,admin_vals.name,settings.API_HOSTNAME,
                                                                          activation_key))
                    else:
                        self.send_mail({ "email": user_email},
                                       self.render_admin(admin_vals.name),html=1,subject="Hello From Affect Lab!")
                        admin_pr = Profile.objects.get(user_id=admin_vals.user_id)
                        self.send_mail({ "email": "info@entropiktech.com"},
                                       self.info_mg1 % (admin_vals.name,admin_pr.company_name,
                                                        admin_pr.designation
                                                        ,admin_vals.email,
                                                        admin_pr.mobile, admin_pr.work_number,
                                                        admin_vals.created_date),subject="User Signup Details")


    def send_email_admin(self, user_email, name):

        # admin=User.objects.get(email=user_email)
        # activation_key = generate_activation_key(user_email)
        # admin.activation_key = activation_key
        # admin.save()
        user = list(User.objects.filter(email=user_email).values())
        if user:
            user = user[0]
            activation_key = generate_activation_key(user_email)
            data = {"user": int(user['id']),
                    "email_verified": False, "activation_key": str(activation_key),
                    "link_date": datetime.datetime.now(), "is_expired": False}
            ids = map(lambda x: x[0], MailActivation.objects.filter(user=int(user['id'])).values_list('id'))
            for i in ids:
                ma = MailActivation.objects.get(pk=i)
                ma.is_expired = True
                ma.save()
            self.send_mail({"email": user_email}, self.admin_msg % (name))
        # data = {"user": int(admin['id']),
        #        "email_verified": False, "activation_key": str(activation_key),
        #        "link_date": datetime.datetime.now(), "is_expired": False}

        # MailActivationSerializer()
        return

    @staticmethod
    def send_mail(request, custom_message=None,html=0,subject=None, from_address = None,reply_to=None, info_mail=1):
        data = dict()

        from_address = settings.DEFAULT_FROM_EMAIL if not from_address else from_address

        toaddrs = request['email']
        message = MIMEMultipart()
        message['from'] = from_address
        message['subject'] = "Action Required" if not subject else subject
        message['to'] = toaddrs
        if info_mail:
            message['bcc'] = "info@entropiktech.com"
        if reply_to:
            message.add_header('reply-to', reply_to)

        toaddrs = [toaddrs,"info@entropiktech.com"] if info_mail else [toaddrs]
        # receiving message
        if custom_message:
            data['message'] = custom_message
        else:
            data['message'] = "Thanks for sending us request, the requested url is %s " % (data['url'])

        if html:
            message.attach(MIMEText(data['message'], 'html'))
        else:
            message.attach(MIMEText(data['message'], 'plain'))
        # setting the smtp server with host, port, user, pwd
        smtp_server = settings.EMAIL_HOST
        smtp_username = settings.EMAIL_HOST_USER
        smtp_password = settings.EMAIL_HOST_PASSWORD
        smtp_port = settings.EMAIL_PORT
        # start connection
        server = smtplib.SMTP(
            host=smtp_server,
            port=smtp_port,
            timeout=10
        )
        server.starttls()
        server.ehlo()
        # login with username and password
        server.login(smtp_username, smtp_password)
        # send mail function call
        # server.sendmail(from_address, toaddrs, message.as_string())

        server.quit()
        return JsonResponse(request, safe=False)



class CreateMediaTest(APIView):
    def send_mail(self, usertester, msg):
        SendMail().send_mail({"type": "ios", "email": usertester[0]}, custom_message=msg)

    def common_function(self,data,ad_clutter=None,update_media_id=None):
        queryflag = 0

        if isinstance(data, QueryDict):
            queryflag = 1
            datalist = dict(data.iterlists())

        start_date = parser.parse(data['start_date'],dayfirst = True)
        end_date = start_date + datetime.timedelta(days=365) #parser.parse(data['end_date'],dayfirst = True)
        # data = dict(json_info.iteritems())
        # , dict(data.dict())
        # print dict(data.iteritems())

        data["usertesters"] = datalist["usertesters"] if queryflag else data["usertesters"]
        data["usertesters"] = [data["usertesters"]] if \
            isinstance(data["usertesters"], str) or isinstance(data["usertesters"], int) else data["usertesters"]
        tags = datalist["tags"] if queryflag else data['tags']
        project_id = data["project_id"]
        media_id = data["media_id"]

        user_id = data["user_id"]
        user_id = get_admin_id(user_id)
        is_completed = False  # bool(data['is_completed'])
        testers = data["usertesters"]
        hashtags = []
        invalid_testers = []
        test_ids = map(lambda x: x[0], MediaTest.objects.filter(media=media_id, project=project_id,
                                                                admin__user_id=user_id, is_completed=False,
                                                                end_date__gt=datetime.datetime.now()).values_list('id'))


        for t in test_ids:
            invalid_testers += map(lambda x: x[0], MediaTesterMapping.objects.filter(media_test=t,
                                                                                     user_tester__user_id__in=testers,
                                                                                     is_completed=False).values_list(
                "user_tester__user_id"))

        testers = set(testers).difference(invalid_testers)
        for i in tags:
            ht = map(lambda x: x[0], HashTag.objects.filter(hashtag=i).values_list("id"))
            if not ht:
                hashtagobject = HashTag(hashtag=i)
                hashtagobject.save()
                hashtag_id = hashtagobject.id
            else:
                hashtag_id = ht[0]
            hashtags.append(hashtag_id)

        try:
            admin = Admin.objects.get(user_id=user_id)
        except:
            return {"status": "Admin not found for user id"}

        media_test_data = {"start_date": start_date, "end_date": end_date, "is_completed": is_completed,
                           "media": media_id,'image':None, "project": project_id, "admin": admin.id, "hashtag": hashtags}
        ad_clutter_data = media_test_data.copy()
        if update_media_id :
            try:
                mid = MediaTest.objects.get(media_id=update_media_id)
                serializer = MediaTestSerializer(mid, data=media_test_data, partial=True)
                for i in ad_clutter:
                    ad_clutter_data.update({'media': i})
                    try:
                        test = MediaTest.objects.get(media_id=i)
                        test_ser=MediaTestSerializer(test,data=ad_clutter_data,partial=True)
                    except:
                        test_ser = MediaTestSerializer(data=ad_clutter_data)
                    if test_ser.is_valid():
                        test_ser.save()
            except:
                return {"status":"Media test for Media Id Not Found"}
        else:
            serializer = MediaTestSerializer(data=media_test_data)

        if serializer.is_valid():
            serializer.save()
        else:
            return {"status": "Failure"}

        # hashtags = HashTag.objects.filter(**{"hashtag__in":tags}).values_list("id")
        for i in hashtags:
            mth = MediaTestHashtagSerializer(data={"mediatest_id": serializer.data['id'],
                                                   "hashtag_id": i})
            if mth.is_valid():
                mth.save()

        mediatesterdict = {}
        mediatesterdict.update({"is_completed": is_completed,
                                "media_test": serializer.data['id'], "created_date": datetime.datetime.now()})


        for i in testers:
            mediatesterdict.update({"user_tester": i})
            usertester = list(UserTester.objects.filter(id=i).values_list('email', 'name'))

            if usertester:
                usertester = usertester[0]
                # SendMail().send_mail({'type':'ios','email':'akshay.hazari@entropika.space'})
                try:
                    media_obj = Media.objects.get(id=media_id)
                    project_obj = Project.objects.get(id=project_id)
                    msg = """Hi %s,                                                                                                                                                                         
                                 The following test has been created for you which is assigned by %s,                                                                                                       
                                    Project : % s                                                                                                                                                           
                                    Media : %s                                                                                                                                                              
                                    Start Date : %s                                                                                                                                                         
                                    End Date : %s                                                                                                                                                           
                                    Hashtags : %s                                                                                                                                                           
                    """ % (usertester[1],admin.name.title(), project_obj.name, media_obj.name, start_date,
                           end_date, ', '.join(tags))

                    t = threading.Thread(target=self.send_mail, args=[usertester, msg])
                    t.setDaemon(False)
                    t.start()
                except:
                    pass

                    # SendMail().send_mail({'type': 'ios','email': userterser[0]})
            mtms = MediaTesterMappingSerializer(data=mediatesterdict)
            if mtms.is_valid():
                mtms.save()
        return dict(status="Done",media_id=media_id,project_id=project_id)

    
    def post(self, request):
        request.POST._mutable = True
        data = request.data
        response = self.common_function(data)
        return Response(response)


class EditMediaTest(APIView):
    def send_mail(self, usertester, msg):
        SendMail().send_mail({"type": "ios", "email": usertester[0]}, custom_message=msg)

    def common_function(self,data):
        queryflag = 0

        if isinstance(data, QueryDict):
            queryflag = 1
            datalist = dict(data.iterlists())

        start_date = parser.parse(data['start_date'],dayfirst = True)
        end_date = parser.parse(data['end_date'],dayfirst = True)
        # data = dict(json_info.iteritems())
        # , dict(data.dict())
        # print dict(data.iteritems())

        data["usertesters"] = datalist["usertesters"] if queryflag else data["usertesters"]
        data["usertesters"] = [data["usertesters"]] if \
            isinstance(data["usertesters"], str) or isinstance(data["usertesters"], int) else data["usertesters"]
        tags = datalist["tags"] if queryflag else data['tags']
        project_id = data["project_id"]
        media_id = data["media_id"]
        user_id = data["user_id"]
        user_id = get_admin_id(user_id)
        is_completed = False  # bool(data['is_completed'])
        testers = data["usertesters"]
        hashtags = []
        invalid_testers = []
        test_ids = map(lambda x: x[0], MediaTest.objects.filter(media=media_id, project=project_id,
                                                                admin__user_id=user_id, is_completed=False,
                                                                end_date__gt=datetime.datetime.now()).values_list('id'))


        for t in test_ids:
            invalid_testers += map(lambda x: x[0], MediaTesterMapping.objects.filter(media_test=t,
                                                                                     user_tester__user_id__in=testers,
                                                                                     is_completed=False).values_list(
                "user_tester__user_id"))

        testers = set(testers).difference(invalid_testers)
        for i in tags:
            ht = map(lambda x: x[0], HashTag.objects.filter(hashtag=i).values_list("id"))
            if not ht:
                hashtagobject = HashTag(hashtag=i)
                hashtagobject.save()
                hashtag_id = hashtagobject.id
            else:
                hashtag_id = ht[0]
            hashtags.append(hashtag_id)

        try:
            admin = Admin.objects.get(user_id=user_id)
        except:
            return Response({"status": "Admin not found for user id"})

        media_test_data = {"start_date": start_date, "end_date": end_date, "is_completed": is_completed,
                           "media": media_id, "project": project_id, "admin": admin.id, "hashtag": hashtags}
        serializer = MediaTestSerializer(data=media_test_data)

        if serializer.is_valid():
            serializer.save()
        else:
            return Response({"status": "Failure"}, status=status.HTTP_200_OK)

        # hashtags = HashTag.objects.filter(**{"hashtag__in":tags}).values_list("id")
        for i in hashtags:
            mth = MediaTestHashtagSerializer(data={"mediatest_id": serializer.data['id'],
                                                   "hashtag_id": i})
            if mth.is_valid():
                mth.save()

        mediatesterdict = {}
        mediatesterdict.update({"is_completed": is_completed,
                                "media_test": serializer.data['id'], "created_date": datetime.datetime.now()})

        for i in testers:
            mediatesterdict.update({"user_tester": i})
            usertester = list(UserTester.objects.filter(id=i).values_list('email', 'name'))

            if usertester:
                usertester = usertester[0]
                # SendMail().send_mail({'type':'ios','email':'akshay.hazari@entropika.space'})
                try:
                    media_obj = Media.objects.get(id=media_id)
                    project_obj = Project.objects.get(id=project_id)
                    msg = """Hi %s,
                                 The following test has been created for you which is assigned by %s,
                                    Project : % s
                                    Media : %s
                                    Start Date : %s
                                    End Date : %s
                                    Hashtags : %s
                    """ % (usertester[1],admin.name.title(), project_obj.name, media_obj.name, start_date,
                           end_date, ', '.join(tags))

                    t = threading.Thread(target=self.send_mail, args=[usertester, msg])
                    t.setDaemon(False)
                    t.start()
                except:
                    pass

                    # SendMail().send_mail({'type': 'ios','email': userterser[0]})
            mtms = MediaTesterMappingSerializer(data=mediatesterdict)
            if mtms.is_valid():
                mtms.save()
        return dict(status="Done")

    def post(self, request):
        request.POST._mutable = True
        data = request.data
        response = self.common_function(data)
        return Response(response)


class CreateMediaTesterMapping(APIView):
    def post(self, request):
        request.POST._mutable = True
        request["created_date"] = parser.parse(request.data['created_date'],dayfirst = True)
        serializer = MediaTesterMappingSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(dict(status="Done"))
        return Response({"status": "Failure"}, status=status.HTTP_200_OK)


class ContentAnalysisUpload(APIView):

    def __init__(self):
        self.valid_keys = ["content_upload_id","second","user_id","attention","meditation","media_id","BP","ET","YF",
                           "Blink","YY","AL","al_type","al_bcq_valid","f2_degree","f2_progress_level","AP","ME","CR",
                           "cr_type","cr_bcq_valid","CP","cp_type","cp_bcq_valid","bp_theta_power","bp_alpha_power",
                           "bp_beta_power","bp_gamma_power","yf_min","yf_max","yf_diff","me_min","me_max","me_diff",
                           "me2_rate","me2_total","me2_changing_rate","timestamp","data_loss",
                           "tg_gamma_low","tg_gamma_high","tg_beta_low","tg_beta_high","tg_theta","tg_delta",
                           "tg_alpha_low","tg_alpha_high"]

    def check_user_exists(self,media_id, user_id):
        try:
            mtm = MediaTesterMapping.objects.get(media_test__media_id=media_id,user_tester__user_id=user_id)
        except:
            mtms = MediaTesterMappingSerializer(data={"media_test__media_id":media_id,"user_tester__user_id":user_id,
                                                "created_date":str(datetime.datetime.now()),"is_completed":False})
            if mtms.is_valid():
                mtms.save()
            #mtm.save()
        return

    def dict_clean(self,content_analysis_dict):
        result = {}

        for key,value in content_analysis_dict.iteritems():
            if value == None:
                value = 0
            result[key] = value
        return result

    def valid_entries(self,data):
        for key in data.keys():
            if key not in self.valid_keys:
                del data[key]


    def localtime_to_utc(self, localtime):
        localtime = parser.parse(localtime,dayfirst = True)
        local = pytz.timezone("Asia/Kolkata")
        local_dt = local.localize(localtime, is_dst = None)
        utc_dt = local_dt.astimezone(pytz.utc)
        return utc_dt


    def post(self, request):
        request.POST._mutable = True
        roundoff_entries = ["attention", "meditation", "media_id", "BP", "ET", "YF", "Blink", "YY", "AL", "AP", "ME",
                            "CR", "CP", "al_type", "al_bcq_valid", "f2_degree", "f2_progress_level", "cr_type",
                            "cr_bcq_valid", "cp_type", "cp_bcq_valid", "bp_theta_power", "bp_alpha_power",
                            "bp_beta_power", "bp_gamma_power", "yf_min", "yf_max", "yf_diff", "me_min", "me_max",
                            "me_diff", "me2_rate", "me2_total", "me2_changing_rate",
                            "tg_gamma_low","tg_gamma_high","tg_beta_low","tg_beta_high",
                            "tg_theta","tg_delta","tg_alpha_low","tg_alpha_high"]
        info = {}
        # try :
        #     info = json.loads(request.body)
        # except:
        #     return Response({"status":"Incorrect Format"})
        info_array = request.data
        media_id = None
        raw_data = []
        content_list = []

        emotion_id_dict = {}
        for elem in EEGEmotion.objects.all().values():
            emotion_id_dict[elem['emotion']] = elem['id']

        flag = 0
        for info in info_array:
            if isinstance(info, QueryDict):
                info = dict(info.iterlists())

            user_id = info['user_id']

            contentupload = info['content_upload']

            contentupload['starttime'] = self.localtime_to_utc(contentupload['starttime'])

            contentupload['endtime'] = self.localtime_to_utc(contentupload['endtime'])

            contentupload['duration'] = (contentupload['endtime']
                                         - contentupload['starttime']).total_seconds()
            contentupload['upload_date'] = self.localtime_to_utc(contentupload['upload_date'])

            contentupload.update({"user_id": user_id})

            contentupload = ContentUploadSerializer(data=contentupload)
            if contentupload.is_valid():
                contentupload.save()
            else:
                return Response({"status": "Failure"}, status=status.HTTP_200_OK)

        #for content in contentanalysis_array:

            contentanalysis_array = info['content_analysis']
            #if not media_id :
            media_id = int(contentanalysis_array[0]['media_id']) if contentanalysis_array else 0
            if not flag:
                self.check_user_exists(media_id,user_id)
                flag=1
            for contentanalysis in contentanalysis_array:
                timestamp = str(datetime.datetime.now())
                if 'timestamp' in contentanalysis:
                    contentanalysis['timestamp'] = str(parser.parse(contentanalysis['timestamp'],
                                                                dayfirst=True).replace(tzinfo=None))
                default = {"attention": 0, "meditation": 0, "media_id": 0, "BP": 0, "ET": 0, "YF": 0, "Blink": 0,"YY":0,
                           "AL": 0, "AP": 0, "ME": 0,"timestamp":timestamp,
                           "CR": 0, "CP": 0, "al_type": 0, "al_bcq_valid": 0, "f2_degree": 0, "f2_progress_level": 0,
                           "cr_bcq_valid": 0, "cp_type": 0, "cp_bcq_valid": 0, "bp_theta_power": 0, "bp_alpha_power": 0,
                           "bp_beta_power": 0, "bp_gamma_power": 0, "yf_min": 0, "yf_max": 0, "yf_diff": 0, "me_min": 0,
                           "me_max": 0,"cr_type": 0,"me_diff": 0,"me2_rate": 0, "me2_total": 0,"me2_changing_rate": 0,
                              "tg_gamma_low":0,"tg_gamma_high":0,"tg_beta_low":0,"tg_beta_high":0,
                            "tg_theta":0,"tg_delta":0,"tg_alpha_low":0,"tg_alpha_high":0}
                curr_raw_data = contentanalysis['raw_data'] if 'raw_data' in contentanalysis else []
                raw_data.append(curr_raw_data)
                contentanalysis = self.dict_clean(contentanalysis)
                default.update(contentanalysis)
                eegemotion = AvgGraph1().make_emotion3(default)[0]
                contentanalysis=default
                contentanalysis.update({"user_id": user_id})
                contentanalysis.update({'content_upload_id': contentupload.data['id']})
                for key in contentanalysis.keys():
                    if key in roundoff_entries:
                        contentanalysis[key] = round(contentanalysis[key], 2) if key in contentanalysis else None
                self.valid_entries(contentanalysis)


                contentanalysis_obj = ContentAnalysis(**contentanalysis)
                contentanalysis_obj.eegemotion_id = emotion_id_dict[eegemotion]
                content_list.append(contentanalysis_obj)

                #contentanalysis = ContentAnalysisSerializer(data=contentanalysis)
                #isvalid = contentanalysis.is_valid()
                #if isvalid:
                #    contentanalysis.save()
                if media_id:
                    try:
                        mtm = map(lambda x: x[0], MediaTesterMapping.objects.filter(user_tester__user_id=user_id, media_test__media_id=media_id, is_completed=False).values_list('id'))
                        for mtm_id in mtm:
                            obj = MediaTesterMapping.objects.get(id=mtm_id)
                            obj.is_completed = True
                            obj.save()
                    except:
                        pass

        ca_ids = ContentAnalysis.objects.bulk_create(content_list)
        raw_obj_list = []

        for i in range(len(ca_ids)):
            raw_obj = RawData(content_analysis=ca_ids[i],data = raw_data[i])
            raw_obj_list.append(raw_obj)
        raw_ids = RawData.objects.bulk_create(raw_obj_list)
                # else:
        # return Response({"status": "Failure"}, status=status.HTTP_400_BAD_REQUEST)
        return Response(dict(status="success"))


class TesterLists(APIView):
    def view_details(self, project_id,media_id):
        try :
            pr_obj = Project.objects.get(id=project_id)
            project_name = pr_obj.name
            cat_name = pr_obj.category.name
            subcat_name = pr_obj.subcategory.name
            #content_name = pr_obj.content_type.name
        except:
            return {}
        response = {}
        complete = []
        incomplete = []

        complete += filter(None, map(lambda x: x[0], MediaTesterMapping.objects.filter(media_test__media_id=media_id,
                                                        is_completed=True).values_list('user_tester_id').distinct()))
        incomplete += filter(None, map(lambda x: x[0], MediaTesterMapping.objects.filter(media_test__media_id=media_id,
                                                        is_completed=False).values_list('user_tester_id').distinct()))
        complete = UserTester.objects.filter(id__in=complete).values('user_id', "name", 'email')
        incomplete = UserTester.objects.filter(id__in=incomplete).values('user_id', "name", 'email')
        response.update({'completed':complete, "incomplete":incomplete, #"content":content_name,
                         "category":cat_name,
                         "sub_category":subcat_name, "project_name":project_name})
        return response

    def post(self, request):
        info = request.data
        #is_closed = bool(info['is_closed'])
        media_id = info['media_id']
        project_id = info['project_id']
        response = self.view_details(project_id,media_id)
        return Response(response)


class TesterLists_1(APIView):
    def view_details(self, user_id, is_completed, media_id, project_id):
        try :
            pr_obj=Project.objects.get(id=project_id)
            project_name = pr_obj.name
            cat_name=pr_obj.category.name
            subcat_name = pr_obj.subcategory.name
            #content_name = pr_obj.content_type.name
        except:
            return {}
        if is_completed:
            mt_ids = list(MediaTest.objects.filter(admin__user=user_id
                                                   , is_completed=is_completed, media_id=media_id,
                                                   project_id=project_id, end_date__lte=datetime.datetime.now()).values(
                'id', 'media_id',
                'project_id', 'end_date', 'start_date',
                'is_completed').distinct())
        else:
            mt_ids = list(MediaTest.objects.filter(admin__user=user_id
                                                   , is_completed=is_completed, media_id=media_id, project_id=project_id
                                                   , end_date__gt=datetime.datetime.now()).values('id').distinct())
        response = {}
        complete = []
        incomplete = []
        for i in mt_ids:
            complete += filter(None, map(lambda x: x[0], MediaTesterMapping.objects.filter(media_test_id=i['id'],
                                                                                           is_completed=True).values_list(
                'user_tester_id').distinct()))
            incomplete += filter(None, map(lambda x: x[0], MediaTesterMapping.objects.filter(media_test_id=i['id'],
                                                                                             is_completed=False).values_list(
                'user_tester_id').distinct()))
        complete = UserTester.objects.filter(id__in=complete).values('user_id', "name", 'email')
        incomplete = UserTester.objects.filter(id__in=incomplete).values('user_id', "name", 'email')
        response.update({'completed': complete, "incomplete": incomplete,
                         #"content":content_name,
                         "category":cat_name,
                         "sub_category":subcat_name,"project":project_name})
        return response

    def post(self, request):
        info = request.data
        is_closed = bool(info['is_closed'])
        user_id = info['user_id']
        media_id = info['media_id']
        project_id = info['project_id']
        response = self.view_details(user_id, is_closed, media_id, project_id)
        return Response(response)


class ScreenRecording(APIView):

    def post(self, request):
        path = os.path.join(settings.PROJECT_DIR + "/static/media/")
        if not os.path.isdir(path):
            os.mkdir(path)
        if not 'request_id' in request.data.keys():
            return Response({"status":'request_id is missing'}, status=status.HTTP_406_NOT_ACCEPTABLE)
        if len(request.FILES) == 0:
            return Response({"status":'file upload is missing'}, status=status.HTTP_406_NOT_ACCEPTABLE)
        try:
            request_id = str(request.data['request_id'])
            path = os.path.join(settings.PROJECT_DIR + "/static/media/facialrecording/" + request_id)
            if not os.path.isdir(path):
                os.mkdir(path)
            filename = os.path.join(settings.PROJECT_DIR + "/static/media/", request_id,
                                    str(request.FILES['file'].name))
            # request.data.pop("media_id")
            media = open(filename, "wb")
            media.write(request.FILES["file"].read())
            media.close()
            return Response({"status": True})
        except:
            return Response({"status": False})


class TestProgressList(APIView):
    def get_test_data(self, user_id, is_completed):
        if is_completed:
            mt_ids = list(MediaTest.objects.filter(admin__user=user_id
                                                   , is_completed=is_completed,
                                                   end_date__lte=datetime.datetime.now()).values('id', 'media_id',
                                                                                                 'project_id',
                                                                                                 'end_date',
                                                                                                 'start_date',
                                                                                                 'is_completed').distinct(
                'media_id'))
        else:
            mt_ids = list(MediaTest.objects.filter(admin__user=user_id
                                                   , is_completed=is_completed,
                                                   end_date__gt=datetime.datetime.now()).values('id', 'media_id',
                                                                                                'project_id',
                                                                                                'end_date',
                                                                                                'start_date',
                                                                                                'is_completed').distinct(
                'media_id'))
        res = []
        for i in mt_ids:
            num_incomplete = MediaTesterMapping.objects.filter(media_test_id=i['id']
                                                               , is_completed=False).count()

            media = Media.objects.filter(id=i['media_id']).values('image_id', 'name')
            image_name, media_name = '', ''
            if len(media):
                image_name = map(lambda x: x[0], list(Image.objects.filter(id=media[0]['image_id']).values_list('image_name')))
                image_name = image_name[0] if image_name else ""
                media_name = media[0]['name']  # if len(media) else ''

            start_date, end_date = i['start_date'], i['end_date']
            tzinfo = end_date.tzinfo
            if datetime.datetime.now(tzinfo) > end_date:
                i['is_completed'] = True
                mtobject = MediaTest.objects.get(id=i['id'])
                mtobject.is_completed = True
                mtobject.save()
            num_completed = MediaTesterMapping.objects.filter(media_test_id=i['id']
                                                              , is_completed=True).count()
            project = Project.objects.get(id=i['project_id'])
            duration = ContentAnalysis.objects.filter(media_id=i['media_id']).aggregate(Max('second'))['second__max']
            duration = duration if duration else 0
            res += [{'media_id': i['media_id'],'project_id':i['project_id'],"media_test_id":i["id"],
                     'media_name': media_name, 'project_name': project.name,
                     'is_completed': i['is_completed'], 'image_name': image_name,
                     'num_incomplete': num_incomplete, 'num_completed': num_completed,
                     'duration': duration, 'start_date': start_date.date(), 'end_date': end_date.date()}]
        return res

    def post(self, request):
        info = request.data
        user_id = get_admin_id(info['user_id'])
        closed_res, live_res = self.get_test_data(user_id, True), self.get_test_data(user_id, False)
        res = {'closed_tests': closed_res, 'live_tests': live_res}
        return Response(res)


class ContentAnalysisInsert(APIView):
    def post(self, request):
        try:
            content_info = json.loads(request.body())
        except ValueError:
            return Response("Unable to process request, incorrect json", status=status.HTTP_200_OK)
        bulk_content = settings.DB.content_analysis.initialize_ordered_bulk_op()
        for content in content_info:
            bulk_content.insert(content)
        try:
            bulk_content.execute({'w': 1})
        except BulkWriteError as bwe:
            return Response({"status": "Bulk Write Error"}, status=status.HTTP_200_OK)
        return Response({"status": "Inserted"}, status=status.HTTP_200_OK)


class HealthCheck(APIView):
    def get(self, request, format=None):
        return Response()


class CheckEmail(APIView):
    def post(self, request):
        return Response(User.objects.filter(email=request.data['email']).count() > 0)


class VideoPlay(APIView):
    # permission_classes = (IsAdminUser,)
    # authentication_classes = (QuietBasicAuthentication,)

    def __init__(self, **kwargs):
        super(VideoPlay, self).__init__(**kwargs)
        self.cmw = None

    def post(self, request):
        try:
            json_info = json.loads(request.body)
        except ValueError:
            return Response(request.body, status=status.HTTP_200_OK)
        self.loginid = json_info["login_id"]
        self.starttime = datetime.datetime.now()
        serializer = Video(login_id=json_info["login_id"],
                           project_name=json_info["project_name"],
                           file_name=json_info["file_name"],
                           Upload_date=self.starttime,
                           video_name=json_info["video_name"],
                           hashtags=json_info["hashtags"],
                           duration=0,
                           starttime=self.starttime,
                           endtime=self.starttime)
        serializer.save()
        self.videoid = serializer.id
        return Response(json_info, status=status.HTTP_200_OK)

    def video_stop(self, request):
        endtime = datetime.datetime.now()
        delta = (endtime - self.starttime).seconds
        Video.objects.filter(_id=self.videoid, login_id=self.loginid).update(duration=delta, endtime=endtime)
        self.cmw.close_socket_connection()
        return HttpResponse("Text only, please.", content_type="text/plain")

    def watch_video_history(self, request, login_id):
        video_list = Video.objects.filter(login_id=login_id)
        serializer = VideoSerializer(video_list, many=True)
        return JsonResponse(serializer.data, safe=False)


class AnalyticsView(TemplateView):
    template_name = 'dashboard/analytics.html'

    def report(self, request, login_id, video_id):
        return render_to_response(self.template_name, {'login_id': login_id, 'video_id': video_id})


class AnalyticsEndpoint(APIView):
    # permission_classes = (IsAdminUser,)
    # authentication_classes = (QuietBasicAuthentication,)

    def __init__(self):
        self.color = {'attention': '0080C0', 'meditation': '008040', 'delta': '808080', 'theta': '800080',
                      'lowAlpha': 'FF8040'
            , 'highAlpha': 'FFFF00', 'lowBeta': 'FF0080', 'highBeta': '0080F0', 'lowGamma': '008FFF',
                      'highGamma': '000000'}

    def init_dict(self, name):
        return {"seriesname": name,
                "color": self.color[name],
                "anchorbordercolor": self.color[name],
                "data": []
                }

    def avg(self, data_list):
        if data_list:
            return sum(data_list) / float(len(data_list))
        return 0

    def insert_dict(self, data_dict, rec, name):
        if name in rec:
            data_dict['data'].append({'value': rec[name]})

    def post(self, request, login_id, video_id):
        mindwaveobjects = MindWave.objects.filter(video_id=video_id, login_id=login_id).values()
        dataset = {}
        attention = self.init_dict("attention")
        meditation = self.init_dict("meditation")
        delta = self.init_dict("delta")
        theta = self.init_dict("theta")
        lowAlpha = self.init_dict("lowAlpha")
        highAlpha = self.init_dict("highAlpha")
        lowBeta = self.init_dict("lowBeta")
        highBeta = self.init_dict("highBeta")
        lowGamma = self.init_dict("lowGamma")
        highGamma = self.init_dict("highGamma")

        for rec in mindwaveobjects:
            self.insert_dict(attention, rec, 'attention')
            self.insert_dict(meditation, rec, 'meditation')
            self.insert_dict(delta, rec, 'delta')
            self.insert_dict(theta, rec, 'theta')
            self.insert_dict(lowAlpha, rec, 'lowAlpha')
            self.insert_dict(highAlpha, rec, 'highAlpha')
            self.insert_dict(lowBeta, rec, 'lowBeta')
            self.insert_dict(highBeta, rec, 'highBeta')
            self.insert_dict(lowGamma, rec, 'lowGamma')
            self.insert_dict(highGamma, rec, 'highGamma')

        get_list = lambda x: int(x['value'])
        attention_avg = self.avg(map(get_list, attention['data']))
        meditation_avg = self.avg(map(get_list, meditation['data']))
        return Response({"dataset": [delta, theta, lowAlpha, highAlpha, lowBeta, highBeta, lowGamma, highGamma],
                         "attention_avg": attention_avg, "meditation_avg": meditation_avg,
                         "dataset_att_med": [attention, meditation]})


class GetAPIKey(APIView):

    def post(self,request):
        keys = list(AdminApiAuth.objects.filter(admin__user_id = request.data['user_id']).values_list("id","api_key"))
        if keys:
            keys.sort(key = lambda x : x[0],reverse=True)
            return Response([keys[0][1]])

        admin_id = GenerateApiKey().checkAdmin(request.data['user_id'])

        if not admin_id:
            return Response([])
        else:
            response = GenerateApiKey().generate_key("default", admin_id)
            if 'api_key' not in response:
                return Response([])
            else:
                return Response(response['api_key'])


class DownloadTest(APIView):

    def get_row(self,field_names,row):
        r = []
        five12 = self.process_raw_data(row['id'])
        for k in field_names:
            r.append(row[k])
        for i in range(512):
            if five12:
                r.append(five12[i])
            else:
                r.append(0)
        return r

    def process_raw_data(self,content_analysis_id):
        raw_values = list(RawData.objects.filter(content_analysis_id=content_analysis_id).values())
        return raw_values[0]['data'] if raw_values else raw_values

    def post(self,request):
        request.POST._mutable = True
        response = HttpResponse(content_type='text/csv')
        media_id = request.data['media_id']
        response['Content-Disposition'] = 'attachment; filename="'+str(media_id)+'.csv"'
        contentanalysis = ContentAnalysis.objects.filter(media_id=media_id).values()
        field_names = [f.name for f in ContentAnalysis._meta.get_fields() if f.name not in
                       ('id','rawdata','eegemotion','content_upload_id')]
        writer = csv.writer(response)
        writer.writerow(field_names+[str(i) for i in range(1, 513)])
        for row in contentanalysis:
            writer.writerow(self.get_row(field_names,row))
        return response

class GeneratePassCode(APIView):

    def generatePassCode(self,media_id,passcode = None):

        passcode = passcode if passcode else ''.join(
            random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(8))
        while MediaPassCode.objects.filter(passcode = passcode).values_list('id',flat=True):
            passcode = passcode if passcode else ''.join(
                random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(8))

        try:
            mpc = MediaPassCode.objects.get(media_id=media_id)
            mpc.passcode = passcode
        except:
            mpc = MediaPassCode(media_id = media_id,passcode=passcode)
        mpc.save()
        return passcode

    def post(self,request):
        passcode = request.data['passcode'] if 'passcode' in request.data else None
        if passcode and len(passcode) < 6:
            return Response({'status':"Please provide a passcode with at least six characters"})
        passcode = self.generatePassCode(request.data['media_id'],passcode=passcode)
        return Response({'status':"success","passcode":passcode})

class CheckPasscode(APIView):

    def post(self,request):
        passcode = request.data['passcode'] if 'passcode' in request.data else None
        try:
            mpc = MediaPassCode.objects.get(media_id=request.data['media_id'])
            return Response({"status":mpc.passcode == passcode})
        except:
            return Response({"status": False})

class GetPasscode(APIView):

    def get(self,request,media_id=None):
        try:
            mpc = MediaPassCode.objects.get(media_id=media_id)
            return Response({"status":mpc.passcode})
        except:
            return Response({"status": 'No Passcode Generated for test, please create one'})


class DetailsByPassCode(APIView):

    def post(self,request):
        passcode = request.data['passcode']
        user_id = request.data['user_id'] if 'user_id' in request.data else None
        user_tester_test_completed = False
        try:
            mpc = MediaPassCode.objects.get(passcode=passcode,media__is_deleted=False,media__project__is_deleted=False)
            media = mpc.media
            media_id = media.id
            activitytype_name = media.activitytype.name
            media_is_default = media.is_default
            media_version = media.media_version
            media_url = media.url
            media_name = media.name
            print media_id
            mediatest = MediaTest.objects.get(media_id=media_id)
            mediatest_is_completed= mediatest.is_completed
            mediatest_id = mediatest.id
            # admin user id
            admin_id = mediatest.admin.user_id
            admin_name = mediatest.admin.name
            project = mediatest.project
            project_name = project.name
            project_id = project.id

            response = {'project_id':project_id,'project_name':project_name,'admin_id':admin_id,
                        'mediatest_id':mediatest_id,'media_name':media_name,'media_url':media_url,
                        'media_version':media_version,'media_is_default':media_is_default,
                        'activitytype_name':activitytype_name,'media_id':media_id,'admin_name':admin_name,
                        'mediatest_is_completed':mediatest_is_completed}

        except:
            return Response({"status": 'No Passcode Generated for test, please create one'})

        try:
            if user_id:
                mtm = MediaTesterMapping.objects.get(user_tester__user_id=user_id, media_test_id=mediatest_id)
                user_tester_test_completed = mtm.is_completed
        except:
            pass
        response.update({'user_tester_test_completed':user_tester_test_completed})

        return Response(response)


[{"content_analysis":[{"attention":83,"meditation":50,"media_id":615,"BP":3.4421799182891846,"ET":0,"YF":49.736900329589844,
"Blink":37,"YY":50,"AL":38.826759338378906,"AP":0,"ME":54.356868743896484,"CR":49.307979583740234,"CP":48.59130859375,
"second":1,"al_type":1,"al_bcq_valid":0,"f2_degree":50,"f2_progress_level":0,"cr_type":1,"cr_bcq_valid":0,"cp_type":1,
"cp_bcq_valid":0,"bp_theta_power":9.607572555541992,"bp_alpha_power":8.265681266784668,"bp_beta_power":4.862231731414795,
"bp_gamma_power":-8.068561437539756E-4,"yf_min":22.43379020690918,"yf_max":72.43379211425781,"yf_diff":49.736900329589844,
"me_min":54.356868743896484,"me_max":54.356868743896484,"me_diff":54.356868743896484,"me2_rate":50,"me2_total":0.0069444444961845875,
"me2_changing_rate":25,"timestamp":"2018-03-27 03:58:52","raw_data":[105,98,103,114,132,144,142,145,132,115,105,99,86,
99,140,165,154,128,85,61,68,86,88,59,36,22,17,21,25,43,67,64,50,39,41,42,38,23,16,16,14,18,20,18,24,35,41,50,59,72,85,85,
69,49,48,64,71,71,69,89,112,121,120,106,91,100,114,107,83,56,56,72,81,84,81,72,64,56,56,67,84,100,104,87,70,68,74,84,76,70,
71,72,82,86,91,99,97,96,93,96,98,97,96,87,92,108,107,80,41,34,58,81,81,88,102,99,72,38,36,52,57,56,40,17,9,20,43,70,82,70,
60,56,56,54,56,60,58,70,83,89,84,82,85,91,96,87,75,67,57,59,65,64,60,57,67,82,96,90,77,65,56,58,70,80,69,48,34,26,29,38,54,
72,79,72,64,66,80,91,88,92,102,97,103,112,99,89,90,89,81,75,85,89,88,84,75,82,89,84,67,53,58,75,85,75,64,57,71,77,61,43,26,
20,25,35,35,36,41,58,64,61,66,59,58,57,36,20,28,39,42,48,58,78,98,99,83,68,66,71,73,71,73,73,83,83,66,48,43,42,35,39,49,55,55,54,57
,51,44,42,32,20,25,39,33,33,53,83,97,82,64,57,73,98,104,100,92,92,100,104,98,88,89,98,96,85,92,108,116,103,88,81,80,71,70,64,56,48,
50,49,40,34,50,83,107,104,74,62,80,90,91,87,81,76,75,84,73,58,45,26,3,-4,-2,-5,-5,-11,-23,-21,-10,8,21,22,48,69,72,68,73,88,101,114,
103,77,60,55,66,73,70,67,73,81,85,86,85,98,118,122,120,106,84,74,91,98,87,76,65,57,67,80,85,89,90,91,82,71,72,69,60,57,69,79,72,
52,33,28,43,59,66,66,68,64,53,56,70,73,59,56,68,65,43,36,52,66,61,53,56,73,86,78,64,67,80,80,68,74,83,86,87,84,74,71,75,76,75,
85,83,66,48,43,50,51,57,72,74,66,49,37,44,50,32,27,35,50,57,58,67,65,64,61,64,72,73,66,53,34,17,23,57,80,72,62,70,92,118,130,
134,130,116,105,119,138,134,107,88,82,88,98,98,82,60,48,40,52,66,70,72,74,63,53,54,51,52,58,68,72,65,54,50,39,43,56,57,56,
59,66,64,64,66,59,54,51,54,52,48,53],"tg_alpha_low":18446,"tg_alpha_high":4175,"tg_beta_low":6730,"tg_beta_high":16777146,
"tg_beta_high":2873,"tg_gamma_high":16777128,"tg_delta":5230,"tg_theta":12399}],"content_upload":{"file_name":"androidtest",
"upload_date":"27-03-2018 09:28:52","starttime":"27-03-2018 09:28:21","endtime":"27-03-2018 09:28:53"},"user_id":61}]



class UploadTestFile(APIView):

    def __init__(self):
        {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16}

    def post(self,request):
        test_name = request.FILES['file'].name
        media_id = request.data['media_id']
        project_id = request.data['project_id']
        data = StringIO((request.FILES['file'].read()).decode())
        names = ['time', 'vid_time', 'signal_level', 'blink', 'attention', 'meditation','yy','et','me','ap', 'zone',
                 'delta','theta','tg_alpha_low','tg_alpha_high','tg_beta_low','tg_beta_high','tg_gamma_low',
                 'tg_gamma_high','raw_count','raw_values']+[str(i) for i in range(513)]
        df = pd.read_csv(data,names = names ,sep="," , skiprows=1, header=None)

        return Response({'status':"haha"})
