"""mindwave URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from views import *
import mail_utils

urlpatterns = [
    url(r'^avggraphs1/?$', AvgGraph1.as_view(), name="AvgGraphs"),
    url(r'^avggraphs/(?P<api_key>[A-Za-z0-9\-]+)/(?P<media_id>[0-9]+)/(?P<selection>[A-Za-z\-]+)/?$', AvgGraph.as_view()
        , name="AvgGraphs"),
    url(r'^avggraphs$', AvgGraph.as_view(), name="AvgGraphs"),
    url(r'^video_upload/(?P<login_id>[0-9]+)/?$', VideoUpload().upload, name="video_upload"),
    # url(r'^connect_head_set/', VideoPlay().connect_think_gear, name="connect_head_set"),
    # url(r'^make_connection/', VideoPlay().make_connection, name="make_connection"),
    url(r'^health/?$', HealthCheck.as_view(), name='HealthCheck'),
    url(r'^watch_video_list/(?P<login_id>[0-9]+)/?$', VideoPlay().watch_video_history, name="watch_video_list"),
    url(r'^video_play/(?P<login_id>[0-9]+)/(?P<name>\w+)/?$', VideoPlay.as_view(), name="video_play"),
    url(r'^video_stop/?$', VideoPlay().video_stop,name="video_stop"),
    url(r'^login/?$', LoginView.as_view(), name="login"),
    url(r'^login_create/?$', LoginCreateView.as_view(), name="login_create"),
    url(r'^save_images/?$', SaveImages.as_view(), name="SaveImages"),
    url(r'^signup/?$', Signup.as_view(), name="SignUp"),
    url(r'^scheduleademo/?$', ScheduleADemoView.as_view(), name="scheduleademo"),
    url(r'^analytics/(?P<login_id>[0-9]+)/(?P<video_id>[0-9]+)/?$', AnalyticsView().report, name="analytics"),
    url(r'^analytics_report/(?P<login_id>[0-9]+)/(?P<video_id>[0-9]+)/?$', AnalyticsEndpoint.as_view(), name="analytics"),
    url(r'^graphs/?$', Graphs1.as_view(), name="graphs"),
    url(r'^runtest/?$', RunTest.as_view(), name="runtest"),
    url(r'^mindwave_save/?$', MindWave_Save.as_view(), name="MindWave_Save"),
    url(r'^createentity/(?P<option>[a-zA-Z0-9]+)/(?P<entity>[a-zA-Z0-9]+)/?$', CreateEntity.as_view(), name="CreateEntity"),
    url(r'^selectproject/?$', SelectProject.as_view(), name="SelectProject"),
    url(r'^projectoverview/?$', ProjectOverview.as_view(), name="ProjectOverview"),
    url(r'^toggle_deactivate_user/?$', Toggle_Deactivate_User.as_view(), name="Toggle_Deactivate_User"),
    url(r'^get_deactivated_status/?$', GetDeactivatedStatus.as_view(), name="GetDeactivatedStatus"),
    url(r'^mediaoverview/?$', MediaOverview.as_view(), name="MediaOverview"),
    url(r'^contentanalysis/?$', ContentAnalysisUpload.as_view(), name="ContentAnalysis"),
    url(r'^createmediatest/?$', CreateMediaTest.as_view(), name="CreateMediaTest"),
    url(r'^media_plus_mediatest/?$', MediaPlusMediatest.as_view(), name="MediaPlusMediatest"),
    url(r'^update_media_plus_mediatest/?$', UpdateMediaPlusMediatest.as_view(), name="UpdateMediaPlusMediatest"),
    url(r'^createmediatestermapping/?$', CreateMediaTesterMapping.as_view(), name="CreateMediaTesterMapping"),
    url(r'^querycategories/?$', QueryCategories.as_view(), name="QueryCategories"),
    url(r'^selectuserproject/?$', SelectUserProject.as_view(), name="SelectUserProject"),
    url(r'^get_user_project_details/(?P<api_key>[A-Za-z0-9\-]+)/?$', UserProjectDetails.as_view(),
        name="UserProjectDetails"),
    url(r'^get_project_list/(?P<api_key>[A-Za-z0-9\-]+)/?$', getProjectList.as_view(),name="getProjectList"),
    url(r'^get_project_list1$', getProjectList1.as_view(), name="getProjectList1"),
    url(r'^get_project_list/(?P<api_key>[A-Za-z0-9\-]+)/(?P<project_id>[A-Za-z0-9\-]+)/?$', getProjectList.as_view()
        ,name="getProjectTestList"),
    url(r'^get_project_list$', getProjectList.as_view(),name="getProjectTestList"),
    url(r'^get_list/(?P<api_key>[A-Za-z0-9\-]+)/(?P<project_id>[A-Za-z0-9\-]+)/?$', getProjectList.as_view(),
        name="getList"),
    url(r'^get_list$', getProjectList.as_view(), name="getList"),
    url(r'^getusertesters/?$', GetUserTesters.as_view(), name="GetUserTesters"),
    url(r'^contentanalysisinsert/?$', ContentAnalysisInsert.as_view(), name="ContentAnalysisInsert"),
    url(r'^createtester/?$', CreateTester.as_view(), name="CreateTester"),
    #url(r'^query_content_types/?$', QueryContentTypes.as_view(), name="QueryContentTypes"),
    url(r'^testermedialist/?$',TesterMediaList.as_view(), name = "TesterMediaList"),
    url(r'^updatetester/?$', UpdateTester.as_view(), name="UpdateTester"),
    url(r'^getemail/?$', GetEmailId.as_view(), name="GetEmailId"),
    url(r'^add_media/?$', AddMedia.as_view(), name="AddMedia"),
    url(r'^check_email/?$', CheckEmail.as_view(), name="CheckEmail"),
    url(r'^get_activity_types/?$', getActivityTypeList.as_view(), name="getActivityTypeList"),
    url(r'^add_client/?$', AddClient.as_view(), name="AddClient"),
    url(r'^add_supported_version/?$', AddVersionSupport.as_view(), name="AddVersionSupport"),
    url(r'^check_supported_version/?$', CheckSupportedVersion.as_view(), name="CheckSupportedVersion"),
    url(r'^delete_media/?$', ISDeleted.as_view(), name="ISDeleted"),
    url(r'^delete_project/?$', ISDeleted.as_view(), name="ISDeleted"),
    url(r'^test_reminder_mail/?$', TestReminderMail.as_view(), name="TestReminderMail"),
    url(r'^forgot_password/?$', ForgotPassword.as_view(), name="ForgotPassword"),
    url(r'^reset_password/?$', ResetPasswordEmail.as_view(), name="ResetPasswordEmail"),
    url(r'^media_bulk_upload/?$', MediaBulkUpload.as_view(), name="MediaBulkUpload"),
    url(r'^test_progress_list/?$', TestProgressList.as_view(), name="TestProgressList"),
    url(r'^activate_admin/(?P<key>.+)/?$', mail_utils.SignUpView().activate_user, name='activate'),
    url(r'^get_mail_activation_user/?$', GetMailActivationUser.as_view(), name="GetMailActivationUser"),
    url(r'^testerlists/?$', TesterLists.as_view(), name="TesterLists"),
    url(r'^testerlists_1/?$', TesterLists_1.as_view(), name="TesterLists"),
    url(r'^check_username_available/?$', CheckUserAvailable.as_view(), name="CheckUserAvailable"),
    url(r'^get_testers_for_media/(?P<media_id>[0-9]+)/?$', Get_Testers_For_Media.as_view(), name="Get_Testers_For_Media"),
    url(r'^test_summary/(?P<api_key>[A-Za-z0-9\-]+)/(?P<media_id>[0-9]+)/(?P<selection>[A-Za-z\-]+)/?$',AvgGraph.as_view(),name="test_summary"),
    url(r'^test_summary$',AvgGraph.as_view(),name="test_summary"),
    url(r'^generate_api_key/?$',GenerateApiKey.as_view(),name="generate_activation_key"),
    url(r'^delete_api_key/?$',DeleteApiKey.as_view(),name="delete_api_key"),
    url(r'^api_wrapper/?$',ApiWrapper.as_view(),name="ApiWrapper"),
    url(r'^get_api_key/?$',GetAPIKey.as_view(),name="get_api_key"),
    url(r'^get_editable_testers/?$',getEditableTesters.as_view(),name="get_editable_testers"),
    url(r'^edit_test/?$',EditTest.as_view(),name="edit_test"),
    url(r'^screen_recording/?$',ScreenRecording.as_view(),name="edit_test"),
    url(r'^get_associated_ids/(?P<media_id>[A-Za-z0-9\-]+)/?$',GetAssociatedIds.as_view(),name="get_associated_ids"),
    url(r'^get_edit_dynamic_form_fill/?$',Get_Edit_Dynamic_Form_Fill.as_view(),name="get_edit_dynamic_form_fill"),
    url(r'^download_test/?$',DownloadTest.as_view(),name="download_test"),
    url(r'^createpseudotester/?$', CreatePseudoTester.as_view(), name="CreatePseudoTester"),
    url(r'^check_passcode/?$', CheckPasscode.as_view(), name="CheckPasscode"),
    url(r'^generate_passcode/?$', GeneratePassCode.as_view(), name="GeneratePassCode"),
    url(r'^get_passcode/(?P<media_id>[A-Za-z0-9\-]+)/?$', GetPasscode.as_view(), name="GetPasscode"),
    url(r'^signup_passcode/?$', SignUpPassCode.as_view(), name="SignUpPassCode"),
    url(r'^upload_test_file/?$', UploadTestFile.as_view(), name="upload_test_file"),
    url(r'^details_by_passcode/?$',DetailsByPassCode.as_view(), name="DetailsByPassCode"),
]
