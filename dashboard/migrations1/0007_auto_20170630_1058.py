# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-06-30 10:58
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0006_auto_20170630_1018'),
    ]

    operations = [
        migrations.CreateModel(
            name='DeviceClient',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='affectlab', max_length=150)),
            ],
        ),
        migrations.AddField(
            model_name='versionsupport',
            name='client',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='dashboard.DeviceClient'),
        ),
    ]
