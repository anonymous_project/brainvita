# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-11-02 09:35
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0015_auto_20170913_1149'),
    ]

    operations = [
        migrations.CreateModel(
            name='AdClutter',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
        ),
        migrations.AddField(
            model_name='media',
            name='is_default',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='adclutter',
            name='associated_media',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='associated_media', to='dashboard.Media'),
        ),
        migrations.AddField(
            model_name='adclutter',
            name='default_media',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='default_media', to='dashboard.Media'),
        ),
    ]
