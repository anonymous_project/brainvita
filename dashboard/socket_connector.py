import socket
import sys
import datetime,json,time
from models import MindWave

sock = None
stop = 1
class ConnectMindwave:

    def __init__(self):
        self.server_address = '127.0.0.1'
        self.port = 13854
        self.sock = None

    def make_socket_connection(self):
        global sock
        if sock:
            sock.close()
        print "make_socket_connection"
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_address = (self.server_address, self.port)
        print >>sys.stderr, 'connecting to %s port %s' % server_address
        sock.connect(server_address)
        message = '{"enableRawOutput": false, "format": "Json"}'
        print >> sys.stderr, 'sending "%s"' % message
        sock.send(message)

    def check_connection(self):
        global sock
        try:
            data = sock.recv(2048).split('\r')
            if not len(data) == 0:
                for raw_data in data:
                    try:
                        data_json = json.loads(raw_data)
                    except ValueError:
                        print "json_error"

                    if 'poorSignalLevel' in data_json:
                        if data_json['poorSignalLevel'] <= 100:
                            return 1
                        else:
                            return 0

        except ValueError:
            print ValueError
            return 0


    def save_socket_data(self,loginid=None,videoid=None):
        try:

            global sock
            global stop
            stop = 1
            while stop:
                data = sock.recv(2048)
                try:

                    print >> sys.stderr, 'received "%s"' % data
                    if data:
                        data_json = json.loads(data)
                        if 'poorSignalLevel' in data_json:
                            if data_json['poorSignalLevel'] <= 100:
                                if videoid and loginid:
                                    try:
                                        serializer = MindWave(login_id=loginid,
                                                              video_id=videoid,
                                                              meditation=data_json['eSense']['meditation'],
                                                              attention=data_json['eSense']['attention'],
                                                              raw_entries="",
                                                              delta=data_json['eegPower']['delta'],
                                                              theta=data_json['eegPower']['theta'],
                                                              lowAlpha=data_json['eegPower']['lowAlpha'],
                                                              highAlpha=data_json['eegPower']['highAlpha'],
                                                              lowBeta=data_json['eegPower']['lowBeta'],
                                                              highBeta=data_json['eegPower']['highBeta'],
                                                              lowGamma=data_json['eegPower']['lowGamma'],
                                                              highGamma=data_json['eegPower']['highGamma'])
                                        serializer.save()
                                    except:
                                        print "DB Error"
                        else:
                            print "blank"
                    time.sleep(0.25)
                except:
                    print 'except'

        finally:
            print >> sys.stderr, 'closing socket'
            sock.close()

    def close_socket_connection(self):
        global stop
        stop = 0
        if sock:
            sock.close()