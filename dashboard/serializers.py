from rest_framework import serializers
from models import Login,Video,MindWave,VideoImage,LoginCreate
from fields import Base64ImageField
from models import *
from django.contrib.auth.models import User

class UserSerializer(serializers.ModelSerializer):

    #user =  serializers.OneToOneField(User,on_delete=models.CASCADE)
    name = serializers.CharField(max_length=250)
    email = serializers.CharField(required=True)
    created_date = serializers.DateTimeField()
    #password = serializers.CharField(max_length=250,default = "affectlab")
    role = serializers.CharField(required=True)

    #username = serializers.CharField(required=True)
    class Meta:
        model = User
        fields = ("id",'name','created_date','role','username','email','password')

    # def create_user(self,rec,request):
    #     if rec['role'] == "admin":
    #         return self.create_admin(rec)
    #     elif rec['role'] == "usertester":
    #         return self.create_tester(rec,request)

    def create_admin(self,rec):
        user = self.create(rec)
        created_date = rec.pop("created_date",None)
        name = rec.pop("name",None)
        user.first_name = name.split('@')[0]
        email = rec.pop("email",None)
        admin = Admin()
        admin.user = user
        admin.email = email
        admin.name = name
        admin.created_date = created_date
        user.save()
        admin.save()
        return user

    def create_tester(self,rec,admin):
        user = self.create(rec)
        created_date = rec.pop("created_date",None)
        name = rec.pop("name",None)
        user.first_name = name.split('@')[0]
        email = rec.pop("email",None)
        birth_date = rec.pop("birth_date",None)
        gender = rec.pop("gender",None)
        usertester = UserTester()
        usertester.user = user
        usertester.admin = admin
        usertester.birth_date = birth_date
        usertester.gender = gender
        usertester.email = email
        usertester.name = name
        usertester.created_date = created_date
        user.save()
        usertester.save()
        return user

    def create(self,rec):
        user = User( email = rec["email"],
            username = rec["username"],
            )
        user.set_password(rec["password"])
        user.is_active = False
        user.save()
        return user

class ScheduleADemoSerializer(serializers.ModelSerializer):
    name = serializers.CharField(max_length=250)
    email = serializers.CharField(required=True)
    company_name = serializers.CharField(max_length=250,required=True)
    company_url = serializers.CharField(max_length=500,default=None,allow_blank=True)
    mobile = serializers.CharField(required=True,max_length=20)
    message = serializers.CharField(default=None,allow_blank=True)
    requested_date = serializers.DateTimeField()

    class Meta:
        model = ScheduleADemo
        fields = ('name','email','company_name','company_url','mobile','message','requested_date')


class AdminApiAuthSerializer(serializers.ModelSerializer):
    class Meta:
        model = AdminApiAuth
        fields = ("__all__")


class UserActiveSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("__all__")


class VersionSupportSerializer(serializers.ModelSerializer):
    class Meta:
        model = VersionSupport
        fields = ("__all__")


class DeviceClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = DeviceClient
        fields = ("__all__")


class UserTesterSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserTester
        fields = ("__all__")


class VideoImageSerializer(serializers.ModelSerializer):
    image = Base64ImageField(
        max_length=None, use_url=True,
    )
    class Meta:
        model = VideoImage
        fields = ("__all__")


class LoginSerializer(serializers.ModelSerializer):

    class Meta:
        model = LoginCreate
        fields = ('__all__')


class VideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Video
        fields = ("__all__")


class MindWaveSerializer(serializers.ModelSerializer):
    class Meta:
        model = MindWave
        fields = ("__all__")


class MediaTestSerializer(serializers.ModelSerializer):
    class Meta:
        model = MediaTest
        fields = ("__all__")

class MediaTesterMappingSerializer(serializers.ModelSerializer):
    class Meta:
        model = MediaTesterMapping
        fields = ("__all__")

class MediaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Media
        fields = ("__all__")


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ("__all__")


# class ContentTypeSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = ContentType
#         fields = ("__all__")


class MediaTestHashtagSerializer(serializers.ModelSerializer):
    class Meta:
        model = MediaTestHashtag
        fields = ("__all__")


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ("__all__")


class SubCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = SubCategory
        fields = ("__all__")


class ImageSerializer(serializers.ModelSerializer):
    image_name = Base64ImageField(
        max_length=None, use_url=True,
    )
    class Meta:
        model = Image
        fields = ("__all__")


class ContentUploadSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContentUpload
        fields = ("__all__")

class MailActivationSerializer(serializers.ModelSerializer):
    class Meta:
        model = MailActivation
        fields = ("__all__")



class ContentAnalysisSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContentAnalysis
        fields = ("__all__")
