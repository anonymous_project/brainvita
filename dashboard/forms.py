from __future__ import unicode_literals
from django.contrib.auth.forms import AuthenticationForm
from django import forms
from authtools import forms as authtoolsforms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, HTML, Field


class SignupForm(forms.Form):

    def __init__(self, email ,*args, **kwargs):
        super(SignupForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()

        self.fields["email"].initial = email
        self.fields["email"].widget.input_type = "email"  # ugly hack

        self.helper.layout = Layout(
            Field('name', placeholder="Enter Full Name", autofocus=""),
            Field('username', placeholder="Username"),
            Field('email', placeholder="Enter Email"),
            Field('password1', placeholder="Enter Password"),
            Field('password2', placeholder="Re-enter Password"),
            Field('age', placeholder="Enter Age"),
            Field('gender', placeholder="Enter Gender"),
            Submit('sign_up', 'Sign up', css_class="btn btn-lg btn-primary btn-block"),
            )

    def save(self, is_active=False):
        """
        Saving the user instance with status as inactive
        """
        user = super(SignupForm, self).save(commit=False)
        user.is_active=is_active
        user.save()
        return user
