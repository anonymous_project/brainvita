# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-11-06 08:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0002_adclutter_test_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='adclutter',
            name='test_id',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='media',
            name='is_default',
            field=models.BooleanField(default=False),
        ),
    ]
