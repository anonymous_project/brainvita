from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django.contrib.postgres.fields import JSONField
from datetime import datetime


class Login(models.Model):
    name = models.CharField(max_length=100, null=True)
    email = models.CharField(max_length=100, unique=True)


class LoginCreate(models.Model):
    email_id = models.CharField(max_length=100, unique=True)
    username = models.CharField(max_length=100, unique=True)
    last_login = models.DateTimeField(null=True)
    version = models.DateTimeField(null=True,default=None)
    device_type = models.DateTimeField(null=True, default=None)


class ScheduleADemo(models.Model):
    name = models.CharField(max_length=250,null=True)
    email = models.CharField(max_length=150, null=True)
    company_name = models.CharField(max_length=250,null=True)
    company_url= models.CharField(max_length=500,default=None)
    mobile= models.CharField(max_length=20,null=True)
    message= models.TextField(default=None)
    requested_date = models.DateTimeField(null=True)


class Image(models.Model):
    #url = models.CharField(max_length=1000,  null=True)
    image_name = models.ImageField( null = True)


class Admin(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=250,null=True)
    email = models.CharField(max_length=100, null=True)
    created_date = models.DateTimeField(null=True)
    # password = models.CharField(max_length=250,default = "affectlab")


# class ContentType(models.Model):
#     name = models.CharField(max_length=250, null=True)
#     admin = models.ForeignKey(Admin, default=None,null = True,on_delete=models.CASCADE)

class Category(models.Model):
    name = models.CharField(max_length=250, null=True)
    admin = models.ForeignKey(Admin, default = 1,on_delete=models.CASCADE)


class SubCategory(models.Model):
    name = models.CharField(max_length=250, null=True)
    category = models.ForeignKey(Category, null=True,on_delete=models.CASCADE)
    admin = models.ForeignKey(Admin,default=None,on_delete=models.CASCADE)

# class ProjectMapping(models.Model):
#     project_id = models.ForeignKey(Project,default=None)
#     user_id = models.ForeignKey(UserTester,default=None)

class Project(models.Model):
    name = models.CharField(max_length=100, null=True)
    created_date = models.DateTimeField(null=True)
    category = models.ForeignKey(Category, null=True,on_delete=models.CASCADE) #models.CharField(max_length=250, null=True)
    subcategory = models.ForeignKey(SubCategory, null=True,on_delete=models.CASCADE) #models.CharField(max_length=250, null=True)
    admin = models.ForeignKey(Admin, default=None,null=True,on_delete=models.CASCADE)
    is_deleted = models.BooleanField(default=False)


class UserTester(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=250,null=True)
    email = models.CharField(max_length=100, null=True)
    created_date = models.DateTimeField(null=True)
    birth_date = models.DateField(auto_now_add=True,null=True)
    gender = models.CharField(max_length=10,null=True)
    admin = models.ForeignKey(Admin, default=None,null=True,on_delete=models.CASCADE)
    # is_deactivated = models.BooleanField(default=False)
    # password = models.CharField(max_length=250,default = "affectlab")


class ActivityType(models.Model):
    name = models.CharField(max_length=250, null=True)
    value = models.IntegerField(default=0)


class Media(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=500, null=True)
    image = models.ForeignKey(Image, null=True,on_delete=models.CASCADE)
    created_date = models.DateTimeField(null=True)
    project = models.ForeignKey(Project, null=True,on_delete=models.CASCADE)
    file_location = models.CharField(max_length=100, null=True)
    media_version = models.CharField(max_length=100, null=True,default="")
    url = models.CharField(max_length=1000, null=True)
    activitytype = models.ForeignKey(ActivityType,null=True)
    is_deleted = models.BooleanField(default=False)
    is_default = models.BooleanField(default=False)


class AdClutter(models.Model):
    default_media = models.ForeignKey(Media, related_name = "default_media",null=True,on_delete=models.CASCADE)
    associated_media = models.ForeignKey(Media, related_name = "associated_media", null=True, on_delete=models.CASCADE)


class HashTag(models.Model):
    hashtag = models.CharField(max_length=250, null=True)


class MediaTest(models.Model):
    project = models.ForeignKey(Project, null=True,on_delete=models.CASCADE)
    media = models.ForeignKey(Media, null=True,on_delete=models.CASCADE)
    start_date = models.DateTimeField(null=True)
    end_date = models.DateTimeField(null=True)
    is_completed = models.BooleanField(default=False)
    admin = models.ForeignKey(Admin,default=None, null=True,on_delete=models.CASCADE)
    hashtag = models.ManyToManyField(HashTag,null=True)


class MediaPassCode(models.Model):
    id = models.AutoField(primary_key=True)
    media = models.ForeignKey(Media, blank=True, null=True,on_delete=models.CASCADE)
    passcode = models.CharField(max_length=100,null=True)



class MediaTestHashtag(models.Model):
    mediatest_id = models.ForeignKey(MediaTest, null=True,on_delete=models.CASCADE)
    hashtag_id = models.ForeignKey(HashTag, null=True,on_delete=models.CASCADE)


class MediaTesterMapping(models.Model):
    media_test = models.ForeignKey(MediaTest, null=True,on_delete=models.CASCADE)
    user_tester = models.ForeignKey(UserTester, null=True,on_delete=models.CASCADE)
    created_date = models.DateTimeField(null=True,default=None)
    is_completed = models.BooleanField(default=False)


class Video(models.Model):
    login_id = models.IntegerField()
    project_name = models.CharField(max_length=100, null=True)
    file_name = models.CharField(max_length=100, null=True)
    Upload_date = models.DateTimeField(null=True)
    duration = models.IntegerField(null=True)
    starttime = models.DateTimeField(null=True)
    video_name = models.CharField(max_length=250, null=True)
    hashtags = models.CharField(max_length=250, null=True)
    endtime = models.DateTimeField(null=True)


class VideoImage(models.Model):
    # video_name=models.CharField(max_length=250)
    image = models.ImageField()


class MailActivation(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    email_verified = models.BooleanField(default=False)
    activation_key = models.CharField(max_length=40)
    link_date = models.DateTimeField(auto_now_add=True)
    is_expired = models.BooleanField(default=False)


class ContentUpload(models.Model):
    user_id = models.IntegerField()
    file_name = models.CharField(max_length=1000, null=True)
    upload_date = models.DateTimeField(null=True)
    duration = models.IntegerField(null=True)
    starttime = models.DateTimeField(null=True)
    endtime = models.DateTimeField(null=True)


class DeviceClient(models.Model):
    name = models.CharField(max_length=150,default="affectlab")


class AdminApiAuth(models.Model):
    admin = models.ForeignKey(Admin,default=None)
    api_key = models.UUIDField(null=True,default = None)
    name = models.CharField(max_length=100,default="default")


class VersionSupport(models.Model):
    version = models.IntegerField(default=0, null=True)
    device_type = models.CharField(max_length=100, null=True)
    is_supported = models.BooleanField(default=True)
    client = models.ForeignKey(DeviceClient,on_delete=models.CASCADE,default=1)
    url = models.CharField(max_length=2500, null=True,default="")
    current_version = models.IntegerField(default=None, null=True)

class EEGEmotion(models.Model):
    emotion = models.CharField(max_length=100, null=True)

class ContentAnalysis(models.Model):
    content_upload_id = models.IntegerField()
    second = models.IntegerField(default=0,null=True)
    user_id = models.IntegerField()
    attention = models.FloatField(default=0,null=True)
    meditation = models.FloatField(default=0,null=True)
    timestamp = models.CharField(max_length=100,blank=True,null=True)
    media_id = models.IntegerField()
    BP = models.FloatField(default=0,null=True)
    ET = models.FloatField(default=0,null=True)
    YF = models.FloatField(default=0,null=True)
    Blink = models.FloatField(default=0,null=True)
    YY = models.FloatField(default=0,null=True)
    AL = models.FloatField(default=0,null=True)
    al_type = models.IntegerField(default=0,null=True)
    al_bcq_valid = models.IntegerField(default=0,null=True)
    f2_degree = models.FloatField(default=0,null=True)
    f2_progress_level = models.IntegerField(default=0,null=True)
    AP = models.FloatField(default=0,null=True)
    ME = models.FloatField(default=0,null=True)
    CR = models.FloatField(default=0,null=True)
    cr_type = models.IntegerField(default=0,null=True)
    cr_bcq_valid = models.IntegerField(default=0,null=True)
    CP = models.FloatField(default=0,null=True)
    cp_type = models.IntegerField(default=0,null=True)
    cp_bcq_valid = models.IntegerField(default=0,null=True)
    bp_theta_power = models.FloatField(default=0,null=True)
    bp_alpha_power = models.FloatField(default=0,null=True)
    bp_beta_power = models.FloatField(default=0,null=True)
    bp_gamma_power = models.FloatField(default=0,null=True)
    yf_min = models.FloatField(default=0,null=True)
    yf_max = models.FloatField(default=0,null=True)
    yf_diff = models.FloatField(default=0,null=True)
    me_min = models.FloatField(default=0,null=True)
    me_max = models.FloatField(default=0,null=True)
    me_diff = models.FloatField(default=0,null=True)
    me2_rate = models.FloatField(default=0,null=True)
    me2_total = models.FloatField(default=0,null=True)
    me2_changing_rate = models.FloatField(default=0,null=True)
    tg_gamma_low=models.FloatField(default=0,null=True)
    tg_gamma_high=models.FloatField(default=0,null=True)
    tg_beta_low=models.FloatField(default=0,null=True)
    tg_beta_high=models.FloatField(default=0,null=True)
    tg_theta=models.FloatField(default=0,null=True)
    tg_delta=models.FloatField(default=0,null=True)
    tg_alpha_low=models.FloatField(default=0,null=True)
    tg_alpha_high=models.FloatField(default=0,null=True)
    data_loss=models.BooleanField(default=False)
    eegemotion = models.ForeignKey(EEGEmotion, null = True)


class RawData(models.Model):
    content_analysis = models.ForeignKey(ContentAnalysis)
    data = JSONField(default={})


class MindWave(models.Model):
    login_id = models.IntegerField()
    media_id = models.IntegerField()
    raw_entries = models.TextField(default="")
    attention = models.IntegerField(default=0)
    meditation = models.IntegerField(default=0)
    delta = models.IntegerField(default=0)
    theta = models.IntegerField(default=0)
    lowAlpha = models.IntegerField(default=0)
    highAlpha = models.IntegerField(default=0)
    lowBeta = models.IntegerField(default=0)
    highBeta = models.IntegerField(default=0)
    lowGamma = models.IntegerField(default=0)
    highGamma = models.IntegerField(default=0)
