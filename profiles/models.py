from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible
import uuid
from django.db import models
from django.conf import settings


class BaseProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL,
                                primary_key=True)
    #phone_number = models.CharField(max_length=20)
    #slug = models.UUIDField(default=uuid.uuid4, blank=True, editable=False)
    # Add more user profile fields here. Make sure they are nullable
    # or with default values
    mobile = models.CharField(null= True,max_length=30)
    work_number = models.CharField(null= True,max_length=30)
    designation = models.CharField(null = True,max_length=100)
    company_name = models.CharField(null=True, max_length=100)
    picture = models.ImageField('Profile picture',
                                upload_to='profile_pics/%Y-%m-%d/',
                                null=True,
                                blank=True)
    bio = models.CharField("Short Bio", max_length=200, blank=True, null=True)
    # email_verified = models.BooleanField("Email verified", default=False)
    # activation_key = models.CharField(max_length=40)

    class Meta:
        abstract = True


@python_2_unicode_compatible
class Profile(BaseProfile):
    class Meta:
        app_label = 'profiles'

    def __str__(self):
        return "{}'s profile".format(self.user)
