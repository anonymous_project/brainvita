from django.conf.urls import url
from profiles import views

urlpatterns = [
    url(r'^me$', views.ShowProfile.as_view(), name='show_self'),
    url(r'^myaccount$', views.ShowAccount.as_view(), name='show_account'),
    url(r'^me/edit$', views.EditProfile.as_view(), name='edit_self'),
    # url(r'^(?P<slug>[\w\-]+)$', views.ShowProfile.as_view(), name='show'),
]
